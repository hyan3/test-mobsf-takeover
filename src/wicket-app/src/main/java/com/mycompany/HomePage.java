package com.mycompany;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.string.StringValue;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import java.util.Arrays;
import java.util.List;

// ruleid: java_xss_rule-WicketXSS
public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;

	public HomePage(final PageParameters parameters) {
		super(parameters);

		StringValue paramValue = parameters.get("input");
		String value = "";
		
		if (!paramValue.isEmpty()) {
			value = paramValue.toString();
		}

		testSinks(value);
	}

	void testSinks(String input) {

		// Label
		add(new Label("message", "Label: Your Input : " + input).setEscapeModelStrings(false));

		// Label
		Component component = new Label("message1", "Component label: " + input);
		component.setEscapeModelStrings(false);
		add(component);

		// MultiLineLabel
		MultiLineLabel multiLineLabel = new MultiLineLabel("multiLineLabel",
				"MultiLineLabel: Line1\nLine2 - Your Input : " + input);
		// ruleid: java_xss_rule-WicketXSS
		multiLineLabel.setEscapeModelStrings(false);
		add(multiLineLabel);

		Form<?> form = new Form<>("form");

		// HiddenField
		HiddenField<String> hiddenField = new HiddenField<>("hiddenField", Model.of("HiddenField " + input));
		hiddenField.setEscapeModelStrings(false);
		form.add(hiddenField);

		// FeedbackPanel
		FeedbackPanel feedbackPanel = new FeedbackPanel("feedback");
		feedbackPanel.setEscapeModelStrings(false);
		form.add(feedbackPanel);

		info("This is an informational message. " + input);
		error("This is an error message. " + input);
		success("This is a success message. " + input);

		// Options for the dropdown
		List<String> options = Arrays.asList("Option 1 " + input, "Option 2 " + input, "Option 3 " + input);

		// Model for the selected value
		Model<String> selectedOption = new Model<>("Option 1 " + input);

		// Create a DropDownChoice and add it to the form
		DropDownChoice<String> dropdown = new DropDownChoice<>("dropdown", selectedOption, options);
		dropdown.setEscapeModelStrings(false);
		form.add(dropdown);

		add(form);

		ModalWindow modalWindow = new ModalWindow("modalWindow");

		// Explicitly disable escaping of the model strings (title)
		// for CVE-2015-5347/
		// ruleid: java_xss_rule-WicketXSS
		modalWindow.setEscapeModelStrings(false);
		modalWindow.setTitle("Potentially Unsafe Title " + input);
		// Optionally, set the content of the ModalWindow
		modalWindow.setContent(new MyModalPanel(modalWindow.getContentId()));

		add(modalWindow);
		// Create a link to show the ModalWindow
		add(new AjaxLink<Void>("showModalLink") {
			@Override
			public void onClick(AjaxRequestTarget target) {
				modalWindow.show(target);
			}
		});

	}

}
