package com.mycompany;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;

public class MyModalPanel extends Panel {
    public MyModalPanel(String id) {
        super(id);
        
        add(new Label("label", "This is a label inside the modal."));
    }
}
