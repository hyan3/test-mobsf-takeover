## commandline

Used for testing simple java sources/sinks that do not require web applications.


### Adding tests

Simply create a new package or find the applicable package under `src/main/java/com/gitlab/<type>` to create the new check.
Create the class that demonstrates the vulnerability. Call the class / method from `commandline/src/main/java/com/gitlab/pomsimple/Main.java`


### Running

For docker:
```
mvn package
docker build --platform=linux/amd64 -t javacommand . && docker run --rm --name javacommand javacommand
```

Run:
```
mvn package
java -jar target/pom-simple-0.0.1-SNAPSHOT.jar
```

Run a single scenario:
```
java -jar target/pom-simple-0.0.1-SNAPSHOT.jar TestLdapInjection
```

Run all scenarios:
```
java -jar target/pom-simple-0.0.1-SNAPSHOT.jar all
```

List all scenarios:
```
java -jar target/pom-simple-0.0.1-SNAPSHOT.jar list
```

For TestSamlIgnoreComments, use the jar-with-dependencies
```
java -jar target/pom-simple-0.0.1-SNAPSHOT-jar-with-dependencies.jar TestSamlIgnoreComments
```

For JWTNoneAlgo, use the jar-with-dependencies
```
java -jar target/pom-simple-0.0.1-SNAPSHOT-jar-with-dependencies.jar JWTNoneAlgoLib1
java -jar target/pom-simple-0.0.1-SNAPSHOT-jar-with-dependencies.jar JWTNoneAlgoLib2
java -jar target/pom-simple-0.0.1-SNAPSHOT-jar-with-dependencies.jar JWTNoneAlgoLib3
```

For TestHardcodedKeys, use the jar-with-dependencies
```
java -jar target/pom-simple-0.0.1-SNAPSHOT-jar-with-dependencies.jar TestHardcodeKeys
```