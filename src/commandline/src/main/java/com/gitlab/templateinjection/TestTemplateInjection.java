package com.gitlab.templateinjection;

import com.gitlab.util.ScenarioRunner;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.tools.generic.EscapeTool;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

// ref: java_templateinjection_rule-TemplateInjection
public class TestTemplateInjection implements ScenarioRunner {

    public void apacheVelocitySafe() {
        // Create a context from the tool manager
        VelocityContext context = new VelocityContext();
        // For demonstration purposes, alternatively configure from a properties file
        context.put("esc", new EscapeTool());
        // For demonstration purposes, create an output buffer
        StringWriter stringWriter = new StringWriter();
        // Get userInput
        String userInput = "${(7*7)}";
        // Use the context to pass in the userInput value
        context.put("userInput", userInput);
        // Pass in the context, the output buffer, a logtag (demo), and the template with userInput
        // making sure to escape it if in the context of HTML.
        // ok: java_templateinjection_rule-TemplateInjection
        Velocity.evaluate(context, stringWriter, "demo", "Hello $esc.html($userInput)");
        // Work with the output buffer
        System.out.println(stringWriter);
    }

    public void apacheVelocityUnsafe(String input) throws Exception {
        // Create a context from the tool manager
        VelocityContext context = new VelocityContext();
        // For demonstration purposes, create an output buffer
        StringWriter stringWriter = new StringWriter();
        // ruleid: java_templateinjection_rule-TemplateInjection
        Velocity.evaluate(context, stringWriter, "demo", "Hello " + input);
        // Work with the output buffer
        System.out.println(stringWriter);
    }

    public void freeMarkerUnsafe(String source) {
        //Freemarker configuration object
        Configuration cfg = new Configuration();
        try {
            //Load template from classpath
            cfg.setTemplateLoader(new ClassTemplateLoader(TestTemplateInjection.class, "/templateInjection"));
            Template template = new Template("test.txt", source, cfg);
            // Build the data-model
            Map<String, Object> data = new HashMap<String, Object>();
            // Console output
            Writer out = new OutputStreamWriter(System.out);
            // ruleid: java_templateinjection_rule-TemplateInjection
            template.process(null, out);
            out.flush();
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    public void freeMarkerUnsafeTwo(String templateName) {
        //Freemarker configuration object
        Configuration cfg = new Configuration();
        try {
            //Load template from classpath
            cfg.setTemplateLoader(new ClassTemplateLoader(TestTemplateInjection.class, "/templateInjection"));
            Template template = cfg.getTemplate(templateName);
            // Build the data-model
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("title", "Hello");
            data.put("description", "Testing Template Injection");
            // Console output
            Writer out = new OutputStreamWriter(System.out);
            // ruleid: java_templateinjection_rule-TemplateInjection
            template.process(data, out);
            out.flush();
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    public void freeMarkerSafe() {
        //Freemarker configuration object
        Configuration cfg = new Configuration();
        try {
            //Load template from classpath
            cfg.setTemplateLoader(new ClassTemplateLoader(TestTemplateInjection.class, "/templateInjection"));
            Template template = cfg.getTemplate("HelloWorld.ftl");
            // Build the data-model
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("title", "Hello");
            data.put("description", "Testing Template Injection");
            // Console output
            Writer out = new OutputStreamWriter(System.out);
            // ok: java_templateinjection_rule-TemplateInjection
            template.process(data, out);
            out.flush();
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() throws Exception {
        apacheVelocitySafe();
        apacheVelocityUnsafe("#set ($run=1 + 1) $run");
        freeMarkerSafe();
        System.out.println();
        // Pass ${\"freemarker.template.utility.Execute\"?new()(\"cat /etc/passwd\")} as the user input to mimic a template injection"
        freeMarkerUnsafe("maliciousCommand");
        System.out.println();
        freeMarkerUnsafeTwo("test.txt");
        System.out.println();
    }
}
