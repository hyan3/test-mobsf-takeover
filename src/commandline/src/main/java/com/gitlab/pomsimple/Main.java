package com.gitlab.pomsimple;

import com.gitlab.crypto.WeakTLSProtocolHttpClient;
import com.gitlab.inject.*;
import com.gitlab.saml.TestSamlIgnoreComments;
import com.gitlab.crypto.RsaNoPadding;
import com.gitlab.crypto.TestJwtDecodeWithoutVerify;
import com.gitlab.crypto.TestJwtDecodeWithoutVerifyBad;
import com.gitlab.crypto.jwtNoneAlgo.*;
import com.gitlab.deserialization.TestXmlDecoder;
import com.gitlab.ftp.FTPInsecureTransport;
import com.gitlab.script.TestScriptInjection;
import com.gitlab.secrets.TestHardcodedKeys;
import com.gitlab.strings.TestBadHexConversion;
import com.gitlab.strings.TestModifyAfterValidation;
import com.gitlab.templateinjection.TestTemplateInjection;
import com.gitlab.xml.TestXsltTransform;
import com.gitlab.util.ScenarioRunner;
import com.gitlab.xpathInjection.TestXpathInjection;

import java.util.Date;
import java.util.Scanner;

public class Main {
	// Add new CLI classes to the list below
	private static final Class<?>[] classesToRun = {
			RsaNoPadding.class,
			TestLdapInjection.class,
			TestModifyAfterValidation.class,
			TestXsltTransform.class,
			TestScriptInjection.class,
			TestXpathInjection.class,
			TestTemplateInjection.class,
			TestBadHexConversion.class,
			TestSamlIgnoreComments.class,
			TestXmlDecoder.class,
			TestJwtDecodeWithoutVerify.class,
			TestJwtDecodeWithoutVerifyBad.class,
			JWTNoneAlgoLib1.class,
			JWTNoneAlgoLib2.class,
			JWTNoneAlgoLib3.class,
			TestHardcodedKeys.class,
			FTPInsecureTransport.class,
			TestSamlIgnoreComments.class,
			WeakTLSProtocolHttpClient.class
	};

	public static void main(String[] args) throws Exception {
		String input = "";
		if (args.length > 0) {
			input = args[0];
			processCommandLine(input);
		} else {
			while (!"exit".equalsIgnoreCase(input)) {
				Scanner scanner = new Scanner(System.in);
				System.out.print(
						"\n Enter the class name to run \n 'all' - run all classes \n 'list' - get all classes \n 'exit' - quit   : ");
				input = scanner.nextLine();
				processCommandLine(input);
			}
		}
	}

	private static void processCommandLine(String input) {
		if ("all".equalsIgnoreCase(input)) {
			// Run all classes
			for (Class<?> clazz : classesToRun) {
				System.out.printf("%s: Starting %s...\n", new Date(), clazz.getSimpleName());
				runClass(clazz);
				System.out.printf("%s: Stopped %s...\n", new Date(), clazz.getSimpleName());
			}
		} else if ("list".equalsIgnoreCase(input)) {
			System.out.println("\n");
			for (Class<?> clazz : classesToRun) {
				System.out.println(clazz.getSimpleName());
			}
		} else {
			// Run the specified class
			for (Class<?> clazz : classesToRun) {
				if (clazz.getSimpleName().equals(input)) {
					System.out.printf("%s: Starting %s...\n", new Date(), clazz.getSimpleName());
					runClass(clazz);
					System.out.printf("%s: Stopped %s...\n", new Date(), clazz.getSimpleName());
				}
			}
		}
	}

	private static void runClass(Class<?> clazz) {
		try {
			// Create an instance of the class and invoke a method
			Object instance = clazz.getDeclaredConstructor().newInstance();
			if (instance instanceof ScenarioRunner) {
				((ScenarioRunner) instance).run();
			}
		} catch (Exception e) {
			// Handle exceptions (skip the class)
			System.err.println("Exception while running class " + clazz.getSimpleName() + ": " + e.getMessage());
		}
	}
}