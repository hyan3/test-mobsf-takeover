// License: LGPL-3.0 License (c) find-sec-bugs
package com.gitlab.xml;

import com.gitlab.util.ScenarioRunner;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import org.xml.sax.SAXException;

//ref: java_xml_rule-XsltTransform
public class TestXsltTransform implements ScenarioRunner {

    public void xsltSafeResource(String input) throws TransformerException {
        System.out.println("\nPerforming xsltSafeResource transformation:");
        System.out.println("\nIn this implementation TransformerFactory FEATURE_SECURE_PROCESSING is set to true. \nTherefore the unsafe input is not processed and instead TransletException is thrown.  \n");

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        TestXsltTransform obj = new TestXsltTransform();

        try {
            InputStream inputStream = obj.getClass().getClassLoader().getResourceAsStream(input);
            Source xslSource = new StreamSource(inputStream);
            // ok: java_xml_rule-XsltTransform
            Transformer transformer = transformerFactory.newTransformer(xslSource);
            Result result = new StreamResult(System.out);

            Source xmlSource = new StreamSource(obj.getClass().getClassLoader().getResourceAsStream("input.xml"));
            transformer.transform(xmlSource, result);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void xsltUnsafeResource(String input) throws TransformerException {

        System.out.println("\nPerforming xsltUnsafeResource transformation: ");
        System.out.println("\nIn this implementation TransformerFactory FEATURE_SECURE_PROCESSING is set to false. \nTherefore the unsafe input is processed and the server attempts to execute a connection to localhost:8090 which causes a connection refused exception. \n");

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, false);
        TestXsltTransform obj = new TestXsltTransform();

        try {
            InputStream inputStream = obj.getClass().getClassLoader().getResourceAsStream(input);
            Source xslSource = new StreamSource(inputStream);
            // ruleid: java_xml_rule-XsltTransform
            Transformer transformer = transformerFactory.newTransformer(xslSource);
            Result result = new StreamResult(System.out);

            Source xmlSource = new StreamSource(obj.getClass().getClassLoader().getResourceAsStream("input.xml"));
            transformer.transform(xmlSource, result);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private Transformer testTransformer(InputStream xslt) {
        try {
            return TransformerFactory.newInstance().newTransformer(new StreamSource(xslt));
        } catch (TransformerConfigurationException tce) {
            System.out.println(tce.getMessage());
        }
        return null;
    }

    public void xsltUnsafeResource2(String input) {
        System.out.println("\nPerforming xsltUnsafeResource2 transformation: ");
        System.out.println("\nIn this implementation testTransformer method directly returns the Transformer. \nThe unsafe input is processed and the server attempts to execute a connection to localhost:8090 which causes a connection refused exception. \n");

        try {
            TestXsltTransform obj = new TestXsltTransform();
            InputStream in = obj.getClass().getClassLoader().getResourceAsStream(input);
            Transformer transformer = testTransformer(in);
            Source xmlSource = new StreamSource(obj.getClass().getClassLoader().getResourceAsStream("input.xml"));
            Result result = new StreamResult(System.out);
            transformer.transform(xmlSource, result);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void xsltUnsafeResource3(String input) throws ParserConfigurationException, IOException, SAXException {
        System.out.println("\nPerforming xsltUnsafeResource3 transformation: ");
        System.out.println("\nIn this implementation transformer use DOMSource as the source for the transformation. \nThere any unsafe input is processed as below. \n");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(input)));
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(new StringWriter());
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            String xmlString = result.getWriter().toString();
            System.out.println(xmlString);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void xsltSafeResource2(String input) {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        TestXsltTransform obj = new TestXsltTransform();

        try {
            InputStream inputStream = obj.getClass().getClassLoader().getResourceAsStream(input);
            Source xslSource = new StreamSource(inputStream);
            // ok: java_xml_rule-XsltTransform
            Transformer transformer = transformerFactory.newTransformer(xslSource);
            Result result = new StreamResult(System.out);

            Source xmlSource = new StreamSource(obj.getClass().getClassLoader().getResourceAsStream("input.xml"));
            transformer.transform(xmlSource, result);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run() throws Exception {
        System.out.println("\nThis test attempts to transform an xsl file that contains an exploit that tries to connect to localhost:8090 using a safe and an unsafe method. ");
        xsltSafeResource("safe.xsl");
        xsltUnsafeResource("safe.xsl");
        xsltUnsafeResource2("safe.xsl");
        xsltUnsafeResource3("<data> <value>Hello, World!</value> </data>");
        xsltSafeResource2("safe.xsl");
    }

}