package com.gitlab.xpathInjection;

import com.gitlab.util.ScenarioRunner;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.io.InputStream;
import java.util.HashMap;

// ref: java_xpathi_rule-XpathInjection
public class TestXpathInjection implements ScenarioRunner{

    public void testOne(String queryInput) {
        try {
            InputStream filePath =  TestXpathInjection.class.getClassLoader().getResourceAsStream("users.xml");
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(filePath);

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();

            // Execute the query
            // ruleid: java_xpathi_rule-XpathInjection
            NodeList users = (NodeList) xpath.evaluate(queryInput, doc, XPathConstants.NODESET);

            for (int i = 0; i < users.getLength(); i++) {
                // ok: java_xpathi_rule-XpathInjection
                String username = xpath.evaluate("username", users.item(i));
                // ok: java_xpathi_rule-XpathInjection
                String password = xpath.evaluate("password", users.item(i));
                System.out.println( "TestOne: " + "Username: " + username + ", Password: " + password);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testTwo(String queryInput) {
        try {
            InputStream filePath =  TestXpathInjection.class.getClassLoader().getResourceAsStream("users.xml");
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(filePath);
            XPath xpath = XPathFactory.newInstance().newXPath();

            // Compile the XPath expression
            // ruleid: java_xpathi_rule-XpathInjection
            XPathExpression expr = xpath.compile(queryInput);

            // Execute the query and get a nodeset
            NodeList users = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

            // Compile expressions for username and password
            // ok: java_xpathi_rule-XpathInjection
            XPathExpression usernameExpr = xpath.compile("username");
            // ok: java_xpathi_rule-XpathInjection
            XPathExpression passwordExpr = xpath.compile("password");

            // Iterate over users and print username and password
            for (int i = 0; i < users.getLength(); i++) {
                String username = (String) usernameExpr.evaluate(users.item(i), XPathConstants.STRING);
                String password = (String) passwordExpr.evaluate(users.item(i), XPathConstants.STRING);
                System.out.println("TestTwo: " + "Username: " + username + ", Password: " + password);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testThree(String queryInput) {
        try {
            InputStream filePath =  TestXpathInjection.class.getClassLoader().getResourceAsStream("users.xml");
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(filePath);
            XPath xpath = XPathFactory.newInstance().newXPath();

            SimpleXPathVariableResolver resolver = new SimpleXPathVariableResolver();
            resolver.setVariable(new QName("user"), queryInput);
            xpath.setXPathVariableResolver(resolver);

            // Compile the XPath expression
            // ok: java_xpathi_rule-XpathInjection
            String result = xpath.compile("//user[contains(., $user)]").evaluate(doc);
            System.out.println("TestThree: 1 - " + result);

            // ruleid: java_xpathi_rule-XpathInjection
            String resultTwo = xpath.compile("//user[username='"+ queryInput +"']").evaluate(doc);
            System.out.println("TestThree: 2 - " + resultTwo);

            String query = "//user[username='"+ queryInput +"']";
            // ruleid: java_xpathi_rule-XpathInjection
            String resultThree = xpath.compile(query).evaluate(doc);
            System.out.println("TestThree: 3 - "  + resultThree);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() throws Exception {
        testOne("//user");
        testTwo("//user");
        testThree("Holmes");
    }
}

class SimpleXPathVariableResolver implements XPathVariableResolver {
    // Use a map or lookup table to store variables for resolution
    private HashMap<QName, String> variables = new HashMap<>();
    // Allow caller to set variables
    public void setVariable(QName name, String value) {
        variables.put(name, value);
    }
    // Implement the resolveVariable to return the value
    @Override
    public Object resolveVariable(QName name) {
        return variables.getOrDefault(name, "");
    }
}