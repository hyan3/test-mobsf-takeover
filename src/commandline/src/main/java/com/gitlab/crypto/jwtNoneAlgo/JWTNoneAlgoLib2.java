package com.gitlab.crypto.jwtNoneAlgo;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.gitlab.util.ScenarioRunner;

// Rule ref: java_crypto_rule_JwtNoneAlgorithm
public class JWTNoneAlgoLib2 implements ScenarioRunner {

    @Override
    public void run() throws Exception {
        bad2();
    }

    public void bad2() throws Exception {
        try {
            // ruleid: java_crypto_rule_JwtNoneAlgorithm
            String token = JWT.create()
                    .withIssuer("server")
                    .withSubject("userId")
                    .sign(Algorithm.none());

            System.out.println("Lib2 bad2() JWT: " + token);
        } catch (Exception e) {
            System.out.println("Exception : " + e.getLocalizedMessage());
        }
    }

}