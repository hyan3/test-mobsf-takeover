package com.gitlab.crypto.jwtNoneAlgo;

import com.gitlab.util.ScenarioRunner;
import javax.crypto.SecretKey;
import io.jsonwebtoken.Jwts;

// Rule ref: java_crypto_rule_JwtNoneAlgorithm
public class JWTNoneAlgoLib1 implements ScenarioRunner {

    @Override
    public void run() throws Exception {
        bad1();
        ok1();
    }

    public void bad1() {
        // ruleid: java_crypto_rule_JwtNoneAlgorithm
        String token = Jwts.builder()
                .subject("Bob")
                .compact();

        System.out.println("Lib1 bad1() JWT: " + token);
    }

    public void ok1() {
        SecretKey key = Jwts.SIG.HS256.key().build();
        // ok: java_crypto_rule_JwtNoneAlgorithm
        String token = Jwts.builder()
                .subject("Bob")
                .signWith(key)
                .compact();

        System.out.println("Lib1 ok() JWT: " + token);
    }

}
