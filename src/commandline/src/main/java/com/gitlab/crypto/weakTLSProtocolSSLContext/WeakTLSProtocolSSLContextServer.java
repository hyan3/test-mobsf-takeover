package com.gitlab.crypto.weakTLSProtocolSSLContext;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;

// The Protocol version will be determined by Java if the version is not specified in SSLContext.getInstance() method.
// By default Java will pick the newest version of the protocols available. This depends on the Java version.
public class WeakTLSProtocolSSLContextServer {
    public static void main(String[] args) {
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            InputStream inputStream =  WeakTLSProtocolSSLContextServer.class.getClassLoader().getResourceAsStream("testcs.keystore");
            ks.load(inputStream, "testcs".toCharArray());
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, "testcs".toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ks);

            // ruleid: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext context = SSLContext.getInstance("SSL");
            context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

            SSLServerSocketFactory factory = context.getServerSocketFactory();
            SSLServerSocket serverSocket = (SSLServerSocket) factory.createServerSocket(8085);
            while (true) {
                SSLSocket socket = (SSLSocket) serverSocket.accept();
                SSLSession session = socket.getSession();
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String line = reader.readLine();
                System.out.println("Received: " + line + " |  Protocol version: " + session.getProtocol());
                reader.close();
                socket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
