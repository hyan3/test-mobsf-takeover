package com.gitlab.crypto.weakTLSProtocolSSLContext;

import javax.net.ssl.*;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.security.KeyStore;

// The Protocol version will be determined by Java if the version is not specified in SSLContext.getInstance() method.
// By default Java will pick the newest version of the protocols available. This depends on the Java version.
public class WeakTLSProtocolSSLContextClient {
    public static void main(String[] args) {
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            InputStream inputStream =  WeakTLSProtocolSSLContextClient.class.getClassLoader().getResourceAsStream("testcs.keystore");
            ks.load(inputStream, "testcs".toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, "testcs".toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(ks);

            // ruleid: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext context = SSLContext.getInstance("SSL");
            context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

            SSLSocketFactory factory = context.getSocketFactory();
            SSLSocket socket = (SSLSocket) factory.createSocket("localhost", 8085);

            PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.println("Hello, server!");
            writer.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
