package com.gitlab.crypto;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.gitlab.util.ScenarioRunner;

public class TestJwtDecodeWithoutVerify implements ScenarioRunner {
    private static final String SECRET = "mysecretkey-mysecretkey-mysecretkey";

    private void verifyToken(String token) {
        Algorithm algorithm = Algorithm.HMAC256(SECRET);
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("auth0")
                .build();

        DecodedJWT jwt2 = verifier.verify(token);
    }

    public String generate() {
        Algorithm algorithm = Algorithm.HMAC256(SECRET);

        return JWT.create()
                .withIssuer("auth0")
                .sign(algorithm);
    }

    public String ok(String token) {
        // ok: java_crypto_rule_JwtDecodeWithoutVerify
        DecodedJWT jwt = JWT.decode(token);
        System.out.println(jwt.getClaims());
        return token;
    }

    @Override
    public void run() throws Exception {
        verifyToken(generate());
        ok(generate());
    }
}

