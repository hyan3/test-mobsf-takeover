package com.gitlab.crypto.jwtNoneAlgo;

import com.gitlab.util.ScenarioRunner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.PlainHeader;
import com.nimbusds.jose.PlainObject;

// Rule ref: java_crypto_rule_JwtNoneAlgorithm
public class JWTNoneAlgoLib3 implements ScenarioRunner {

    @Override
    public void run() throws Exception {
        bad3();
        bad4();
    }

    public void bad3() {
        try {
            // PlainHeaders are set with None Algo.
            // ruleid: java_crypto_rule_JwtNoneAlgorithm
            PlainHeader header = new PlainHeader();

            Payload payload = new Payload("{\"iss\":\"abc\", \"exp\":1300819111}");
            PlainObject plainObject = new PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad3() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bad4() {
        try {
            // PlainHeaders are set with None Algo.
            // ruleid: java_crypto_rule_JwtNoneAlgorithm
            PlainHeader header = new PlainHeader.Builder().contentType("text/plain")
                    .customParam("exp", "1300819111")
                    .build();

            Payload payload = new Payload("{\"iss\":\"abc\"}");
            PlainObject plainObject = new PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad4() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
