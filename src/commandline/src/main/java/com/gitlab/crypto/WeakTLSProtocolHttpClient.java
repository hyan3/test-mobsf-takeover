package com.gitlab.crypto;

import com.gitlab.util.ScenarioRunner;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;

public class WeakTLSProtocolHttpClient implements ScenarioRunner {

    public void testOne() {
        // ruleid: java_crypto_rule-WeakTLSProtocol-DefaultHttpClient
        DefaultHttpClient httpClient = new DefaultHttpClient();
        try {
            HttpGet getRequest = new HttpGet("https://restcountries.com/v3.1/name/deutschland");
            HttpResponse httpResponse = httpClient.execute(getRequest);
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                System.out.println(EntityUtils.toString(entity));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
    }

    public void testTwo() {
        // ok: java_crypto_rule-WeakTLSProtocol-DefaultHttpClient
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://restcountries.com/v3.1/name/deutschland"))
                .build();

        client.sendAsync(request, java.net.http.HttpResponse.BodyHandlers.ofString())
                .thenApply(java.net.http.HttpResponse::body)
                .thenAccept(System.out::println)
                .join();
    }

    @Override
    public void run() throws Exception {
        System.out.println("Response from DefaultHttpClient:");
        testOne();
        System.out.println("\nResponse from java.net.http.HttpClient:");
        testTwo();
    }
}
