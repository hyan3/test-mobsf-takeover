package com.gitlab.crypto;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.gitlab.util.ScenarioRunner;

public class TestJwtDecodeWithoutVerifyBad implements ScenarioRunner {

    private void bad() {
        try {
            Algorithm algorithm = Algorithm.none();

            String token = JWT.create()
                    .withIssuer("auth0")
                    .sign(algorithm);
            // ruleid: java_crypto_rule_JwtDecodeWithoutVerify
            DecodedJWT jwt = JWT.decode(token);
            System.out.println(jwt.getClaims());
        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Couldn't convert Claims.
        }
    }

    @Override
    public void run() throws Exception {
        bad();
    }
}
