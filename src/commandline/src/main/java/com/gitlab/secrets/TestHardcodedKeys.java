package com.gitlab.secrets;

import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyRep;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.DSAPrivateKeySpec;
import java.security.spec.ECFieldFp;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPrivateKeySpec;
import java.security.spec.EllipticCurve;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAMultiPrimePrivateCrtKeySpec;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.DHPrivateKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.kerberos.KerberosKey;
import javax.security.auth.kerberos.KerberosPrincipal;
import javax.security.auth.kerberos.KerberosTicket;

import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.gitlab.util.ScenarioRunner;

import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;

// Rule ref: java_password_rule-HardcodeKey
public class TestHardcodedKeys implements ScenarioRunner {

    private final byte[] key_hardcoded_bytes = "sdf@!#$verf^wv%6Fwe%$$#FFGwfsdefwfe135s$^H)dg"
            .getBytes(Charset.defaultCharset());
    private final String key_hardcoded_string = "sdf@!#$verf^wv%6Fwe%$$#FFGwfsdefwfe135s$^H)dg";

    String keyBigIntegerString = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
    BigInteger p = new BigInteger(
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
    BigInteger q = new BigInteger("1234567890123456789012345678901234567890123456789");
    BigInteger g = new BigInteger(
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");

    String secret11 = "secret";
    byte[] myvar11 = "my secret".getBytes();

    @Override
    public void run() throws Exception {

        try {

            testDESKeySpec(key_hardcoded_bytes);
            testDESedeKeySpec(key_hardcoded_bytes);
            testDHPrivateKeySpec(key_hardcoded_bytes, p);
            testDSAPrivateKeySpec(key_hardcoded_bytes, g, keyBigIntegerString);
            testECPrivateKeySpec(key_hardcoded_bytes, g);
            testKerberosKey(key_hardcoded_bytes);
            testKerberosTicket(key_hardcoded_bytes, keyBigIntegerString);
            testKeyRep(key_hardcoded_bytes, key_hardcoded_string);
            testPKCS8EncodedKeySpec(key_hardcoded_bytes);
            testRSAMultiPrimePrivateCrtKeySpec(g);
            testRSAPrivateCrtKeySpec(g);
            testRSAPrivateKeySpec(p, g);
            testSecretKeySpec(key_hardcoded_bytes);
            testX509EncodedKeySpec(key_hardcoded_bytes);
            testAlgorithmHMAC(key_hardcoded_string, key_hardcoded_bytes);
            testKeyParameterBouncyCastle(key_hardcoded_bytes);
            testHmacUtils(key_hardcoded_bytes, key_hardcoded_string);

        } catch (Exception e) {
            System.out.println("Exception details: " + e.toString());
        }

    }

    public void testDESKeySpec(byte[] keyB) throws Exception {
        System.out.println("Running testDESKeySpec()");
        // ok: java_password_rule-HardcodeKey
        DESKeySpec obj = new DESKeySpec(keyB);

        // ruleid: java_password_rule-HardcodeKey
        DESKeySpec obj2 = new DESKeySpec(key_hardcoded_bytes, 0);

        KeyGenerator keyGenerator = KeyGenerator.getInstance("DES");
        SecretKey secretKey = keyGenerator.generateKey();

        // Convert the SecretKey to byte array
        byte[] desKey = secretKey.getEncoded();

        // Initialize DESKeySpec with the generated key
        // ok: java_password_rule-HardcodeKey
        DESKeySpec desKeySpec = new DESKeySpec(desKey);

    }

    public void testDESedeKeySpec(byte[] keyB) throws Exception {
        System.out.println("Running testDESedeKeySpec()");

        // ok: java_password_rule-HardcodeKey
        DESedeKeySpec obj = new DESedeKeySpec(keyB);

        // ruleid: java_password_rule-HardcodeKey
        DESedeKeySpec obj2 = new DESedeKeySpec(key_hardcoded_bytes, 0);
    }

    public void testKerberosKey(byte[] keyB) throws Exception {
        System.out.println("Running testKerberosKey()");

        try {
            String principalName = "username@REALM.COM";
            KerberosPrincipal principal = new KerberosPrincipal(principalName);
            // ok: java_password_rule-HardcodeKey
            KerberosKey key = new KerberosKey(principal, keyB, 1, 1);

            // ruleid: java_password_rule-HardcodeKey
            KerberosKey key2 = new KerberosKey(principal, key_hardcoded_bytes, 1, 1);
        } catch (Exception e) {
            System.out.println("testKerberosKey Exception:" + e.toString());
        }
    }

    public void testSecretKeySpec(byte[] keyB) throws Exception {

        System.out.println("Running testSecretKeySpec()");
        // ok: java_password_rule-HardcodeKey
        SecretKeySpec obj = new SecretKeySpec(keyB, "AES");

        // ruleid: java_password_rule-HardcodeKey
        SecretKeySpec obj2 = new SecretKeySpec(key_hardcoded_bytes, "AES");
    }

    public void testX509EncodedKeySpec(byte[] keyB) throws Exception {

        System.out.println("Running testX509EncodedKeySpec()");

        // ok: java_password_rule-HardcodeKey
        X509EncodedKeySpec obj = new X509EncodedKeySpec(keyB);

        // ruleid: java_password_rule-HardcodeKey
        X509EncodedKeySpec obj2 = new X509EncodedKeySpec(key_hardcoded_bytes);
    }

    public void testPKCS8EncodedKeySpec(byte[] keyB) throws Exception {

        System.out.println("Running testPKCS8EncodedKeySpec()");
        // ok: java_password_rule-HardcodeKey
        PKCS8EncodedKeySpec obj = new PKCS8EncodedKeySpec(keyB);

        // ruleid: java_password_rule-HardcodeKey
        PKCS8EncodedKeySpec obj2 = new PKCS8EncodedKeySpec(key_hardcoded_bytes);
    }

    public void testKeyRep(byte[] keyB, String keyS) throws Exception {

        System.out.println("Running testKeyRep()");

        // ok: java_password_rule-HardcodeKey
        KeyRep obj = new KeyRep(KeyRep.Type.PRIVATE, "AES", "RAW", keyB);

        // ruleid: java_password_rule-HardcodeKey
        KeyRep obj2 = new KeyRep(KeyRep.Type.PRIVATE, "AES", "RAW", key_hardcoded_bytes);

        // ok: java_password_rule-HardcodeKey
        KeyRep obj3 = new KeyRep(KeyRep.Type.PRIVATE, "AES", "RAW", keyS.getBytes());

        // ruleid: java_password_rule-HardcodeKey
        KeyRep obj4 = new KeyRep(KeyRep.Type.PRIVATE, "AES", "RAW", key_hardcoded_string.getBytes());

    }

    public void testKerberosTicket(byte[] keyB, String keyS) throws Exception {

        System.out.println("Running testKerberosTicket()");

        try {
            KerberosPrincipal clientPrincipal = new KerberosPrincipal("client@REALM.COM");
            KerberosPrincipal serverPrincipal = new KerberosPrincipal("service@REALM.COM");

            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(128); // Key size
            SecretKey sessionKey = keyGen.generateKey();
            Date startTime = new Date();
            Date endTime = new Date(startTime.getTime() + 24 * 60 * 60 * 1000); // 1 day validity

            byte[] ticket = new byte[1];

            InetAddress[] addresses = new InetAddress[1];
            addresses[0] = InetAddress.getByName("127.0.0.1"); 

            // ok: java_password_rule-HardcodeKey
            KerberosTicket kerberosTicket = new KerberosTicket(
                    ticket,
                    clientPrincipal,
                    serverPrincipal,
                    keyB,
                    1,
                    new boolean[1],
                    startTime,
                    endTime,
                    startTime,
                    endTime,
                    addresses);

            // ok: java_password_rule-HardcodeKey
            KerberosTicket kerberosTicket2 = new KerberosTicket(
                    ticket,
                    clientPrincipal,
                    serverPrincipal,
                    keyS.getBytes(),
                    1,
                    new boolean[1],
                    startTime,
                    endTime,
                    startTime,
                    endTime,
                    addresses);

            // ruleid: java_password_rule-HardcodeKey
            KerberosTicket kerberosTicket3 = new KerberosTicket(
                    ticket,
                    clientPrincipal,
                    serverPrincipal,
                    key_hardcoded_bytes,
                    1,
                    new boolean[1],
                    startTime,
                    endTime,
                    startTime,
                    endTime,
                    addresses);

            // ruleid: java_password_rule-HardcodeKey
            KerberosTicket kerberosTicket4 = new KerberosTicket(
                    ticket,
                    clientPrincipal,
                    serverPrincipal,
                    key_hardcoded_string.getBytes(),
                    1,
                    new boolean[1],
                    startTime,
                    endTime,
                    startTime,
                    endTime,
                    addresses);
        } catch (Exception e) {
            System.out.println("testKerberosTicket Exception:" + e.toString());
        }
    }

    public void testDSAPrivateKeySpec(byte[] keyB, BigInteger keyBI, String keyS) throws Exception {

        System.out.println("Running testDSAPrivateKeySpec()");

        // ok: java_password_rule-HardcodeKey
        DSAPrivateKeySpec obj = new DSAPrivateKeySpec(keyBI, p, q, g);

        // ok: java_password_rule-HardcodeKey
        DSAPrivateKeySpec obj2 = new DSAPrivateKeySpec(new BigInteger(keyB), p, q, g);

        // ruleid: java_password_rule-HardcodeKey
        DSAPrivateKeySpec obj3 = new DSAPrivateKeySpec(new BigInteger(key_hardcoded_bytes), p, q, g);

        // ruleid: java_password_rule-HardcodeKey
        DSAPrivateKeySpec ob4 = new DSAPrivateKeySpec(new BigInteger(keyBigIntegerString), p, q, g);

        // ok: java_password_rule-HardcodeKey
        DSAPrivateKeySpec ob5 = new DSAPrivateKeySpec(new BigInteger(keyS), p, q, g);

    }

    public void testDHPrivateKeySpec(byte[] keyB, BigInteger keyBI) throws Exception {

        System.out.println("Running testDHPrivateKeySpec()");

        // ok: java_password_rule-HardcodeKey
        DHPrivateKeySpec obj = new DHPrivateKeySpec(keyBI, p, g);

        // ok: java_password_rule-HardcodeKey
        DHPrivateKeySpec obj2 = new DHPrivateKeySpec(new BigInteger(keyB), p, g);

        // ruleid: java_password_rule-HardcodeKey
        DHPrivateKeySpec obj3 = new DHPrivateKeySpec(new BigInteger(key_hardcoded_bytes), p, g);

        // ruleid: java_password_rule-HardcodeKey
        DHPrivateKeySpec ob4 = new DHPrivateKeySpec(new BigInteger(keyBigIntegerString), p, g);

    }

    public void testECPrivateKeySpec(byte[] keyB, BigInteger keyBI) throws Exception {

        System.out.println("Running testECPrivateKeySpec()");

        BigInteger p = new BigInteger("FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF", 16);
        BigInteger a = new BigInteger("FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC", 16);
        BigInteger b = new BigInteger("5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B", 16);
        BigInteger gx = new BigInteger("6B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C296", 16);
        BigInteger gy = new BigInteger("4FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5", 16);
        BigInteger n = new BigInteger("FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551", 16);
        int h = 1;

        ECFieldFp field = new ECFieldFp(p);
        EllipticCurve curve = new EllipticCurve(field, a, b);
        ECPoint g = new ECPoint(gx, gy);
        ECParameterSpec ecSpec = new ECParameterSpec(curve, g, n, h);

        // ok: java_password_rule-HardcodeKey
        ECPrivateKeySpec ecPrivateKeySpec1 = new ECPrivateKeySpec(keyBI, ecSpec);

        // ok: java_password_rule-HardcodeKey
        ECPrivateKeySpec ecPrivateKeySpec2 = new ECPrivateKeySpec(new BigInteger(keyB), ecSpec);

        // ruleid: java_password_rule-HardcodeKey
        ECPrivateKeySpec ecPrivateKeySpec3 = new ECPrivateKeySpec(new BigInteger(key_hardcoded_bytes), ecSpec);

        // ruleid: java_password_rule-HardcodeKey
        ECPrivateKeySpec ecPrivateKeySpec4 = new ECPrivateKeySpec(new BigInteger(keyBigIntegerString), ecSpec);

        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PrivateKey privateKey = keyFactory.generatePrivate(ecPrivateKeySpec1);

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC");
        kpg.initialize(new ECGenParameterSpec("secp256r1"), new SecureRandom());
        KeyPair keyPair = kpg.generateKeyPair();
        BigInteger s = ((java.security.interfaces.ECPrivateKey) keyPair.getPrivate()).getS();
        ECParameterSpec params = ((java.security.interfaces.ECPrivateKey) keyPair.getPrivate()).getParams();

        // ok: java_password_rule-HardcodeKey
        ECPrivateKeySpec ecPrivateKeySpec = new ECPrivateKeySpec(s, params);

    }

    public void testRSAPrivateKeySpec(BigInteger nParam, BigInteger dParam) throws Exception {

        System.out.println("Running testRSAPrivateKeySpec()");

        BigInteger n = new BigInteger(
                "00c277691f6b8b8bdc8f56c416f5da1eaf9573e0978b37801c10e87d2aed3b5e9f828b97648b73fd1202ddc1dcb92a9a2e8b1a746f4c8f2b8a492d105e8b8c3cb9",
                16); // modulus
        BigInteger d = new BigInteger(
                "74196d3e4fb6a5b50e5242a9dba9d5f4629e3a21b2c1a53ac5b0e959cacd3b3fcb8eaa8bc5e0b7fbd79f2fe154a3ab7af3ebf6f5e5058e4f3eb370acb8d8f0d5",
                16); // private exponent

        // ruleid: java_password_rule-HardcodeKey
        RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(n, d);

        // ok: java_password_rule-HardcodeKey
        RSAPrivateKeySpec rsaPrivateKeySpec2 = new RSAPrivateKeySpec(nParam, dParam);

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048); // 2048 bits is a common RSA key size
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

        // ok: java_password_rule-HardcodeKey
        RSAPrivateKeySpec rsaPrivateKeySpec11 = new RSAPrivateKeySpec(privateKey.getModulus(),
                privateKey.getPrivateExponent());

    }

    public void testRSAMultiPrimePrivateCrtKeySpec(BigInteger mod) throws Exception {

        System.out.println("Running testRSAMultiPrimePrivateCrtKeySpec()");

        BigInteger modulus = new BigInteger("aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899", 16);

        BigInteger publicExponent = new BigInteger(
                "aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899", 16);
        BigInteger privateExponent = new BigInteger(
                "aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899", 16);
        BigInteger primeP = new BigInteger("aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899", 16);
        BigInteger primeQ = new BigInteger("aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899", 16);
        BigInteger primeExponentP = new BigInteger("aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899",
                16);
        BigInteger primeExponentQ = new BigInteger("aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899",
                16);
        BigInteger crtCoefficient = new BigInteger("aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899",
                16);

        // ruleid: java_password_rule-HardcodeKey
        RSAMultiPrimePrivateCrtKeySpec rsaKeySpec = new RSAMultiPrimePrivateCrtKeySpec(
                modulus, publicExponent, privateExponent, primeP, primeQ, primeExponentP, primeExponentQ,
                crtCoefficient, null);

        // ok: java_password_rule-HardcodeKey
        RSAMultiPrimePrivateCrtKeySpec rsaKeySpec2 = new RSAMultiPrimePrivateCrtKeySpec(
                mod, publicExponent, privateExponent, primeP, primeQ, primeExponentP, primeExponentQ,
                crtCoefficient, null);

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048); // 2048 bits is a common key size for RSA
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        RSAPrivateCrtKey privateKey = (RSAPrivateCrtKey) keyPair.getPrivate();

        BigInteger modulus2 = privateKey.getModulus();
        BigInteger publicExponent2 = privateKey.getPublicExponent();
        BigInteger privateExponent2 = privateKey.getPrivateExponent();
        BigInteger primeP2 = privateKey.getPrimeP();
        BigInteger primeQ2 = privateKey.getPrimeQ();
        BigInteger primeExponentP2 = privateKey.getPrimeExponentP();
        BigInteger primeExponentQ2 = privateKey.getPrimeExponentQ();
        BigInteger crtCoefficient2 = privateKey.getCrtCoefficient();

        // ok: java_password_rule-HardcodeKey
        RSAPrivateCrtKeySpec rsaPrivateCrtKeySpec = new RSAPrivateCrtKeySpec(
                modulus2, publicExponent2, privateExponent2,
                primeP2, primeQ2, primeExponentP2, primeExponentQ2, crtCoefficient2);

    }

    public void testRSAPrivateCrtKeySpec(BigInteger mod) throws Exception {

        System.out.println("Running testRSAPrivateCrtKeySpec()");

        BigInteger modulus = new BigInteger("aabbccddeeff00112233445566778899aabbccddeeff00112233445566778899", 16);
        BigInteger publicExponent = new BigInteger("a23bccddeeff00112233445566778899aabbccddeeff00112233445566778899",
                16);
        BigInteger privateExponent = new BigInteger("a45bccddeeff00112233445566778899aabbccddeeff00112233445566778899",
                16);
        BigInteger primeP = new BigInteger("a346eff00112233445566778899aabbccddeeff00112233445566778899", 16);
        BigInteger primeQ = new BigInteger("a567eff00112233445566778899aabbccddeeff00112233445566778899", 16);
        BigInteger primeExponentP = new BigInteger("aa3456deeff00112233445566778899aabbccddeeff00112233445566778899",
                16);
        BigInteger primeExponentQ = new BigInteger("a4567eff00112233445566778899aabbccddeeff00112233445566778899",
                16);
        BigInteger crtCoefficient = new BigInteger("345ff00112233445566778899aabbccddeeff00112233445566778899",
                16);

        // ruleid: java_password_rule-HardcodeKey
        RSAPrivateCrtKeySpec rsaPrivateCrtKeySpec = new RSAPrivateCrtKeySpec(
                modulus, publicExponent, privateExponent,
                primeP, primeQ, primeExponentP, primeExponentQ, crtCoefficient);

        // ok: java_password_rule-HardcodeKey
        RSAPrivateCrtKeySpec rsaPrivateCrtKeySpec2 = new RSAPrivateCrtKeySpec(
                mod, publicExponent, privateExponent,
                primeP, primeQ, primeExponentP, primeExponentQ, crtCoefficient);

    }

    public void testAlgorithmHMAC(String secretKey, byte[] safeBytes) {

        System.out.println("Running testAlgorithmHMAC()");

        String secret1 = "secret";
        byte[] secretBytes = "my secret".getBytes();
        try {
            // ruleid:java_password_rule-HardcodeKey
            Algorithm algorithm = Algorithm.HMAC256("secret");
            // ruleid:java_password_rule-HardcodeKey
            Algorithm algorithm2 = Algorithm.HMAC256(secret1);
            // ruleid:java_password_rule-HardcodeKey
            Algorithm algorithm3 = Algorithm.HMAC256(secretBytes);
            // ruleid:java_password_rule-HardcodeKey
            Algorithm algorithm4 = Algorithm.HMAC256("my secret".getBytes());
            // ok:java_password_rule-HardcodeKey
            Algorithm algorithm5 = Algorithm.HMAC256(secretKey);
            // ruleid:java_password_rule-HardcodeKey
            Algorithm algorithm6 = Algorithm.HMAC256(secret11);
            // ruleid:java_password_rule-HardcodeKey
            Algorithm algorithm7 = Algorithm.HMAC256(myvar11);
            // ruleid:java_password_rule-HardcodeKey
            Algorithm algorithm8 = Algorithm.HMAC256(secret11.getBytes());
            // ok:java_password_rule-HardcodeKey
            Algorithm algorithm9 = Algorithm.HMAC256(secretKey.getBytes());
            // ok:java_password_rule-HardcodeKey
            Algorithm algorithm10 = Algorithm.HMAC256(safeBytes);
            // ruleid:java_password_rule-HardcodeKey
            Algorithm algorithm11 = Algorithm.HMAC384("secret");
            // ruleid:java_password_rule-HardcodeKey
            Algorithm algorithm12 = Algorithm.HMAC512("secret");

            String token = JWT.create()
                    .withIssuer("auth0")
                    .sign(algorithm);
        } catch (Exception exception) {
            System.out.println("Exception:" + exception.toString());
        }
    }

    public void testKeyParameterBouncyCastle(byte[] key) {

        System.out.println("Running testKeyParameterBouncyCastle()");

        HMac hMac = new HMac(new SHA512Digest());
        // ok: java_password_rule-HardcodeKey
        hMac.init(new KeyParameter(key));
        // ruleid: java_password_rule-HardcodeKey
        hMac.init(new KeyParameter(key_hardcoded_bytes));
    }

    public void testHmacUtils(byte[] key, String keyS) {

        System.out.println("Running testHmacUtils()");

        HmacAlgorithms algorithm = HmacAlgorithms.HMAC_SHA_256;

        // ok: java_password_rule-HardcodeKey
        HmacUtils a = new HmacUtils(algorithm, key);

        // ok: java_password_rule-HardcodeKey
        HmacUtils b = new HmacUtils(algorithm, keyS);

        // ruleid: java_password_rule-HardcodeKey
        HmacUtils c = new HmacUtils(algorithm, key_hardcoded_bytes);

        // ruleid: java_password_rule-HardcodeKey
        HmacUtils d = new HmacUtils(algorithm, key_hardcoded_string);
    }

}