package com.test.servlet.inject;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// Case 1: http://localhost:8080/ServletSample/TestSeamLogInjection?input=Title
// Case 2: http://localhost:8080/ServletSample/TestSeamLogInjection?input=#{book.getTitle()}

@Name("testSeamLogInjection")
public class TestSeamLogInjection extends HttpServlet {
    Log log = Logging.getLog(TestSeamLogInjection.class);

    @In
    private Book book;

    public TestSeamLogInjection() {
        this.book = new Book();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
        log.info("Book title ="+book.getTitle());
        log.info("Book title (from user) =#{book.getTitle()}");
        log.info("Book title (from user) ="+ input);

        try (PrintWriter out = resp.getWriter()) {
            out.println("RequestParamToHeader Test: " + resp);
        }
    }

    // Case 3: http://localhost:8080/ServletSample/TestSeamLogInjection
    /*
     *
        POST /ServletSample/TestSeamLogInjection HTTP/1.1
        Host: localhost:8080
        Content-Type: application/x-www-form-urlencoded
        Content-Length: 15

        input=#{1000*5}
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String input = request.getParameter("userInput");
        log.info("Book title ="+book.getTitle());
        log.info("Book title (EL) =#{book.getTitle}");
        log.info("This is user controlled input ="+ input);
        log.info("This is user controlled input (safe) = #0", input);

        try (PrintWriter out = response.getWriter()) {
            out.println("RequestParamToHeader Test: " + response);
        }
    }

}