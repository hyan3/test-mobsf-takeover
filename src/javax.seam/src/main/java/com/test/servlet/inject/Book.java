package com.test.servlet.inject;

import org.jboss.seam.annotations.Name;

@Name("book")
public class Book {

    public Book() {

    }

    private String title = "Test title";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
