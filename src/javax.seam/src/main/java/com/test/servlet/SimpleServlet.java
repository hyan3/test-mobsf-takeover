package com.test.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.text.StringEscapeUtils;

public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public SimpleServlet() {
		
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Get user input
		String htmlInput = request.getParameter("userInput");
		// Encode the input using the Html4 encoder
		String htmlEncoded = StringEscapeUtils.escapeHtml4(htmlInput);
		// Force the HTTP resonse to be content type of text/plain so it is not interpreted as HTML
		response.setContentType("text/plain"); 
		// Ensure UTF-8
		response.setCharacterEncoding("UTF-8");
		// Write response
		response.getWriter().write(htmlEncoded);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
}
