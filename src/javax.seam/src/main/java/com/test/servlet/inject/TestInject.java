package com.test.servlet.inject;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstServlet
 */
/**
 * @author preetham
 *
 */
public class TestInject extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public TestInject() {
		
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Get user input
		String userInput = request.getParameter("userInput");
		response.setContentType("text/plain"); 
		// Ensure UTF-8
		response.setCharacterEncoding("UTF-8");
		// Write response
		response.getWriter().write(userInput);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
	



}
