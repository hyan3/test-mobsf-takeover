## javax.seam

Used for testing web applications source/sinks based on the older javax.seam namespace.

### Adding tests

Simply create a new package or find the applicable package under `src/main/java/com/gitlab/<type>` to create the new class.
Create the class that demonstrates the vulnerability. 

Add the servlet to the src/main/webapp/WEB-INF/web.xml:
```
  ...
  <servlet>
	<servlet-name>TestInject</servlet-name>
	<display-name>TestInject</display-name>
	<description>Test GET, POST methods of Servlet</description>
	<servlet-class>com.test.servlet.inject.TestInject</servlet-class>
  </servlet>

  <servlet-mapping>
	<servlet-name>TestInject</servlet-name>
	<url-pattern>/TestInject</url-pattern>
  </servlet-mapping>
```

### Running

Run:
```
mvn package
docker build -t javaxseam . && docker run --rm -p 8080:8080 --name javaxseam javaxseam
# Once started, hit the endpoint, all servlets are under the "/ServletSample" application namespace.
curl -vvv "http://localhost:8080/ServletSample/TestInject?input=test"
```