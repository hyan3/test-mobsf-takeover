<%--
  Created by IntelliJ IDEA.
  User: ChathuminaVi_q5lryrm
  Date: 2/6/2024
  Time: 8:41 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hidden View</title>
</head>
<body>
<h3>FileDisclosure Vulnerability</h3>
<p>
    In Spring MVC, the ModelAndView class looks up a view by name to resolve a `.jsp`
    file. <br/> If this view name comes from user-supplied input, it could be abused to attempt
    to return a JSP view that the user should not have access to.
</p>
</body>
</html>
