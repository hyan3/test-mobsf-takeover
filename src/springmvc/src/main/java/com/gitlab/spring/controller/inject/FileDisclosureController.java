package com.gitlab.spring.controller.inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DateFormat;
import java.util.*;


@Controller
public class FileDisclosureController {

	/**
	 * case 1: http://localhost:8080/spring-mvc-example/FileDisclosure/caseOne?input=hiddenView
	 * case 2: http://localhost:8080/spring-mvc-example/FileDisclosure/caseTwo?input=hiddenView
	 * case 3: http://localhost:8080/spring-mvc-example/FileDisclosure/caseThree?input=hiddenView
	 * case 4: http://localhost:8080/spring-mvc-example/FileDisclosure/caseFour?input=hiddenView
	 * case 5: http://localhost:8080/spring-mvc-example/FileDisclosure/caseFive?input=hiddenView
	 * case 6: http://localhost:8080/spring-mvc-example/FileDisclosure/caseSix?input=key1
	 */
	@RequestMapping(value = "/FileDisclosure/caseOne", method = RequestMethod.GET)
	public ModelAndView mvc(HttpServletRequest request, HttpServletResponse response, Model model) {
		String input = request.getParameter("input");
		// ruleid: java_inject_rule-FileDisclosureSpringFramework
		ModelAndView mv = new ModelAndView(input);
		System.out.println(input + " view requested.");
		return mv;
	}

	@RequestMapping(value = "/FileDisclosure/caseTwo", method = RequestMethod.GET)
	public ModelAndView mvcTwo(HttpServletRequest request, HttpServletResponse response, Model model) {
		String input = request.getParameter("input");
		// ok: java_inject_rule-FileDisclosureSpringFramework
		ModelAndView mv = new ModelAndView("home", input, new Object());
		System.out.println(input + " view requested.");
		return mv;
	}

	@RequestMapping(value = "/FileDisclosure/caseThree", method = RequestMethod.GET)
	public ModelAndView mvcThree(HttpServletRequest request, HttpServletResponse response, Model model) {
		String[] paramValues = {};
		Enumeration<String> parameterNames = request.getParameterNames();
		if (parameterNames.hasMoreElements()) {
			String firstParamName = parameterNames.nextElement();
			paramValues = request.getParameterValues(firstParamName);
		}
		// ruleid: java_inject_rule-FileDisclosureSpringFramework
		ModelAndView mv = new ModelAndView(paramValues[0], "home", new Object());
		System.out.println(paramValues[0] + " view requested.");
		return mv;
	}

	@RequestMapping(value = "/FileDisclosure/caseFour", method = RequestMethod.GET)
	public ModelAndView mvcFour(HttpServletRequest request, HttpServletResponse response, Model model) {
		String[] parameterValues = request.getParameterValues("input");
		if (parameterValues != null && parameterValues.length > 0) {
			// ruleid: java_inject_rule-FileDisclosureSpringFramework
			ModelAndView mv = new ModelAndView(parameterValues[0], new HashMap());
			System.out.println(parameterValues[0] + " view requested.");
			return mv;
		}

        return null;
    }

	@RequestMapping(value = "/FileDisclosure/caseFive", method = RequestMethod.GET)
	public ModelAndView mvcFive(HttpServletRequest request, HttpServletResponse response, Model model) {
		Map<String, String[]> parameterMap = request.getParameterMap();
		if (!parameterMap.isEmpty()) {
			Map.Entry<String, String[]> firstEntry = parameterMap.entrySet().iterator().next();
			String[] firstParamValues = firstEntry.getValue();
			// ruleid: java_inject_rule-FileDisclosureSpringFramework
			ModelAndView mv = new ModelAndView(firstParamValues[0]);
			System.out.println(firstParamValues[0] + " view requested.");
			return mv;
		}

        return null;
    }

	@RequestMapping(value="/FileDisclosure/caseSix", method=RequestMethod.GET)
	public ModelAndView mvcSix(HttpServletRequest request, HttpServletResponse response, Model model)
	{
		// Create a look up table or pull from a data source
		HashMap<String, String> lookupTable = new HashMap<>();
		lookupTable.put("key1", "hiddenView");
		// Get user input
		String input = request.getParameter("input");
		// Look up view from the user input
		String viewValue = lookupTable.getOrDefault(input, "home");
		// return the new model and view
		// ok: java_inject_rule-FileDisclosureSpringFramework
		return new ModelAndView(viewValue);
	}
}
