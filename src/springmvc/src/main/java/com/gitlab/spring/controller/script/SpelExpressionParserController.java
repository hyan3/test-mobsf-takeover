package com.gitlab.spring.controller.script;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gitlab.spring.model.User;

// ref:java_script_rule-SpelView
@Controller
public class SpelExpressionParserController {

    @RequestMapping(value = "/script/TestSpelExpressionParser", method = RequestMethod.GET)
    public String spelHome(Locale locale, Model model) {
        return "script/spelExpressionParser";
    }

    //Expression Parser that can be used to execute expressions within the standard evaluation context
    @RequestMapping(value = "/script/spel-vulnerable", method = RequestMethod.POST)
    public String spelSimpleEval(@Validated User user, Model model) {
        // Create the Expression Parser
        SpelExpressionParser parser = new SpelExpressionParser();
        // Parse the expression with user provided input, which may not be safe
        // ruleid: java_script_rule-SpelView
        org.springframework.expression.Expression parsedExpression = parser.parseExpression(user.getUserName());

        Object result = parsedExpression.getValue();
        user.setUserName(result.toString());
        model.addAttribute("userName", user.getUserName());

        System.out.println(result);

        return "user";
    }

    //This is an expression parser that uses the SimpleEvaluationContext which reduces the scope of expressions that can be parsed
    //However, direct user input is stil processed as a simple expression, which means that an adversary could execute mathematical and logical expressions
    //This can trigger errors such as dividesByZero
    @RequestMapping(value = "/script/spel-svc", method = RequestMethod.POST)
    public String spel(@Validated User user, Model model) {
        // Create the Expression Parser
        SpelExpressionParser parser = new SpelExpressionParser();
        // Parse the expression with user provided input, which may not be safe
        // ruleid: java_script_rule-SpelView
        org.springframework.expression.Expression parsedExpression = parser.parseExpression(user.getUserName());

        SimpleEvaluationContext context = SimpleEvaluationContext.forReadWriteDataBinding().build();
        Object result = parsedExpression.getValue(context);
        user.setUserName(result.toString());
        model.addAttribute("userName", user.getUserName());

        System.out.println(result);

        return "user";
    }
}