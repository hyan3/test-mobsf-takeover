// License: LGPL-3.0 License (c) find-sec-bugs
package com.example.servlets.cookie;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

//In this scenario: carriage returns and line feeds cannot be used in response cookies regardless of source
//This vulnerability is no longer exploitable as Tomcat and jetty have implemented RFC6565 Compliance

// ref: java_cookie_rule-HttpResponseSplitting
// Case 1: curl --location 'http://localhost:8080/ServletSample/HttpResponseSplitting?input=%5Cr%5Cn' \--header 'input: \r\n' \
@WebServlet(name = "HttpResponseSplitting", value = "/HttpResponseSplitting")
public class HttpResponseSplitting extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public HttpResponseSplitting() {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Parameters and Headers may contain malicous input from outside sources

        //Static Test for unencoded string
        Cookie cUnencoded = new Cookie("name", "\r\n");
        try {
            //cookie not allowed in response: exception triggered
            resp.addCookie(cUnencoded);
        } catch (Exception e) {
            System.out.println("Static unencoded string test error: " + e.getMessage());
        }

        //Static Test for escaped encoded string
        Cookie cUnencodedEscaped = new Cookie("name", "\\r\\n");
        try {
            //cookie not allowed in response: exception triggered
            resp.addCookie(cUnencodedEscaped);
        } catch (Exception e) {
            System.out.println("Static encoded escaped string test error: " + e.getMessage());
        }

        //Static Test for encoded string
        Cookie cEncoded = new Cookie("name", "%5Cr%5Cn");
        try {
            //cookie allowed in response
            resp.addCookie(cEncoded);
        } catch (Exception e) {
            System.out.println("Static encoded string test error: " + e.getMessage());
        }

        //Adding cookies from request parameter:
        try {
            String input = req.getParameter("input");
            Cookie c = new Cookie("name", null);
            //ruleid: java_cookie_rule-HttpResponseSplitting
            c.setValue(input);
            c.setHttpOnly(true);
            c.setSecure(true);
            resp.addCookie(c);
        } catch (Exception e) {
            resp.addHeader("Add-From-Param-Error", e.getMessage());
        }

        //Adding cookies from request headers:
        try {
            String header = req.getHeader("input");
            Cookie cHeader = new Cookie("name", null);
            //ruleid: java_cookie_rule-HttpResponseSplitting
            cHeader.setValue(header);
            cHeader.setHttpOnly(true);
            cHeader.setSecure(true);
            resp.addCookie(cHeader);
        } catch (Exception e) {
            resp.addHeader("Add-From-Header-Error", e.getMessage());
        }

    }

    private String getString(String s) {
        return s;
    }
}
