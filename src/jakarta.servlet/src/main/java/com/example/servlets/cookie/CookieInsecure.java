package com.example.servlets.cookie;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

// ref: java_cookie_rule-CookieInsecure
// Case 1: http://localhost:8080/Servlets-Sample/CookieInsecure?input=danger
// Case 2: http://localhost:8080/Servlets-Sample/CookieInsecure?input=danger2
// Case 3: http://localhost:8080/Servlets-Sample/CookieInsecure?input=safe
@WebServlet(name = "CookieInsecure", value = "/CookieInsecure")
public class CookieInsecure extends HttpServlet {

    private static final String TESTKEY1 = "Testkey1";
    private static final String TESTKEY2 = "Testkey2";
    private static final String TESTKEY3 = "Testkey3";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        boolean TESTKEY1_exists = false;
        boolean TESTKEY2_exists = false;
        boolean TESTKEY3_exists = false;

        String input = request.getParameter("input");
        String message = "Invalid input parameter!";

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals(TESTKEY1)) {
                    TESTKEY1_exists = true;
                } else if (c.getName().equals(TESTKEY2)) {
                    TESTKEY2_exists = true;
                } else if (c.getName().equals(TESTKEY3)) {
                    TESTKEY3_exists = true;
                }
            }
        }

        if (input != null) {
            if (input.equals("danger")) {
                if (TESTKEY1_exists) {
                    message = "Cookie already added! (name: Testkey1) with Secure flag set to false.";
                } else {
                    danger(response);
                    message = "Cookie added (name: Testkey1) with Secure flag set to false.";
                }
            } else if (input.equals("danger2")) {
                if (TESTKEY2_exists) {
                    message = "Cookie already added! (name: Testkey2) without explicitly setting Secure flag.";
                } else {
                    danger2(response);
                    message = "Cookie added (name: Testkey2) without explicitly setting Secure flag.";
                }
            } else if (input.equals("safe")) {
                if (TESTKEY3_exists) {
                    message = "Cookie already added! (name: Testkey3) with Secure flag set to true.";
                } else {
                    safe(response);
                    message = "Cookie added (name: Testkey3) with Secure flag set to true.";
                }
            } else {
                message = "Invalid input parameter!";
            }
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("message: " + message);
        }

    }

    protected void danger(HttpServletResponse response) {
        // ruleid: java_cookie_rule-CookieInsecure
        Cookie myCookie = new Cookie(TESTKEY1, "Testvalue1");
        myCookie.setSecure(false);
        myCookie.setMaxAge(60);
        response.addCookie(myCookie);
    }

    protected void danger2(HttpServletResponse response) {
        // ruleid: java_cookie_rule-CookieInsecure
        Cookie myCookie = new Cookie(TESTKEY2, "Testvalue2");
        myCookie.setMaxAge(60);
        response.addCookie(myCookie);
    }

    protected void safe(HttpServletResponse response) {
        // rule ok: java_cookie_rule-CookieInsecure
        Cookie myCookie = new Cookie(TESTKEY3, "Testvalue3");
        myCookie.setSecure(true);
        myCookie.setMaxAge(60);
        response.addCookie(myCookie);
    }
}
