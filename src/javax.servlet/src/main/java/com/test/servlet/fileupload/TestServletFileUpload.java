package com.test.servlet.fileupload;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

// showcasing the vulnerability where an attacker overwrites a critical file by using path traversal attack through filename.
// Rule: java_file_rule-FileUploadFileName
public class TestServletFileUpload {

    public void handleFileUpload(HttpServletRequest req) throws Exception {
        ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
        List<FileItem> fileItems = upload.parseRequest(req);

        for (FileItem item : fileItems) {
            if (!item.isFormField()) { // This is a file.
                // ruleid: java_file_rule-FileUploadFileName
                String fileName = item.getName();
                System.out.println("Saving " + fileName);

                // Save the file
                File uploadedFile = new File("/data/" + fileName);
                item.write(uploadedFile);
            }
        }
    }
}
