package com.test.servlet.inject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

// Try a file name in the resources folder as an input parameter for both of the test cases
// Case 1: http://localhost:8080/ServletSample/FileDisclosureRequestDispatcher?input=index.html&type=a
// Case 2: http://localhost:8080/ServletSample/FileDisclosureRequestDispatcher?input=index.html&type=b
// Case 3: http://localhost:8080/ServletSample/FileDisclosureRequestDispatcher?input=index.html&type=c
// Case 4: http://localhost:8080/ServletSample/FileDisclosureRequestDispatcher?input=index.html&type=d
// The above cases are vulnerable with a URL such as: http://localhost:8080/ServletSample/FileDisclosureRequestDispatcher?input=WEB-INF/web.xml&type=a

// Safe scenario
// Case 5: http://localhost:8080/ServletSample/FileDisclosureRequestDispatcher?key=key1&type=e

// Unsafe POST scenario
// Case 6: http://localhost:8080/ServletSample/FileDisclosureForm.jsp

public class TestFileDisclosureRequestDispatcher extends HttpServlet {

    public TestFileDisclosureRequestDispatcher() {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String scenario = req.getParameter("type");
            String input = "";
            switch (scenario) {
                case "a": {
                    input = getParameterUsage(req);
                    RequestDispatcher requestDispatcher = req.getSession().getServletContext().getRequestDispatcher("/" + input);
                    // ruleid: java_inject_rule-FileDisclosureRequestDispatcher
                    requestDispatcher.forward(req, resp);
                    break;
                }
                case "b": {
                    input = getParameterNamesUsage(req);
                    RequestDispatcher requestDispatcher = req.getSession().getServletContext().getRequestDispatcher("/" + input);
                    // ruleid: java_inject_rule-FileDisclosureRequestDispatcher
                    requestDispatcher.forward(req, resp);
                    break;
                }
                case "c": {
                    input = getParameterValuesUsage(req);
                    RequestDispatcher requestDispatcher = req.getSession().getServletContext().getRequestDispatcher("/" + input);
                    // ruleid: java_inject_rule-FileDisclosureRequestDispatcher
                    requestDispatcher.forward(req, resp);
                    break;
                }
                case "d": {
                    input = getParameterMapUsage(req);
                    RequestDispatcher requestDispatcher = req.getSession().getServletContext().getRequestDispatcher("/" + input);
                    // ruleid: java_inject_rule-FileDisclosureRequestDispatcher
                    requestDispatcher.forward(req, resp);
                    break;
                }
                case "e": {
                    // Create a look up table or pull from a data source
                    HashMap<String, String> lookupTable = new HashMap<>();
                    lookupTable.put("key1", "/ServletSample/simpleForm.jsp");
                    // Get user input
                    String userInput = req.getParameter("key");
                    // Look up resource to redirect to from the user input
                    String redirectValue = lookupTable.getOrDefault(userInput, "/ServletSample/index.html");
                    // Redirect the user
                    // ok: java_inject_rule-FileDisclosureRequestDispatcher
                    resp.sendRedirect(redirectValue);
                }
            }
        } catch (Exception e) {
            resp.addHeader("Add-From-Param-Error", e.getMessage());
        }
    }

    public String getParameterUsage(HttpServletRequest req) {
        return req.getParameter("input");
    }

    public String getParameterNamesUsage(HttpServletRequest req) {
        Enumeration<String> parameterNames = req.getParameterNames();
        if (parameterNames.hasMoreElements()) {
            String firstParamName = parameterNames.nextElement();
            String[] firstParamValues = req.getParameterValues(firstParamName);
            return firstParamValues[0];
        }
        return "";
    }

    public String getParameterValuesUsage(HttpServletRequest req) {
        String[] parameterValues = req.getParameterValues("input");
        if (parameterValues != null && parameterValues.length > 0) {
            return parameterValues[0];
        }
        return "";
    }

    public String getParameterMapUsage(HttpServletRequest req) {
        Map<String, String[]> parameterMap = req.getParameterMap();
        if (!parameterMap.isEmpty()) {
            Map.Entry<String, String[]> firstEntry = parameterMap.entrySet().iterator().next();
            String[] firstParamValues = firstEntry.getValue();
            return firstParamValues[0];
        }
        return "";
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String filename = req.getParameter("filename");
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(filename);
        // ruleid: java_inject_rule-FileDisclosureRequestDispatcher
        requestDispatcher.include(req, resp);
    }
}