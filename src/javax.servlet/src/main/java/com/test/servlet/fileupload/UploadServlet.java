package com.test.servlet.fileupload;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// Rule: java_file_rule-FileUploadFileName
@WebServlet(name = "UploadServlet", value = "/UploadServlet")
public class UploadServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            new TestServletFileUpload().handleFileUpload(request);
            response.getWriter().write("TestServletFileUpload : File uploaded successfully.");
        } catch (Exception e) {
            response.getWriter().write("File upload failed: " + e.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Forward to the HTML page
        RequestDispatcher dispatcher = request.getRequestDispatcher("/uploadTest1.html");
        dispatcher.forward(request, response);
        return;

    }
}
