package com.test.servlet.inject;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlInjection extends HttpServlet {

    DataSource dataSource;

    public SqlInjection(){
        try {
            System.out.println("SqlInjection: Initializing H2 for SqlInjection");
            InitialContext context = new InitialContext();
            dataSource = (DataSource) context.lookup("java:comp/env/jdbc/H2DataSource");
            initH2();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    //Test sql injection using Apache Torque BasePeer

    //Valid CURL:
    //curl --location --request GET 'http://localhost:8080/ServletSample/SqlInjection' \
    //--header 'input: D-Artagnan'

    //Malicious CURL:
    //curl --location --request GET 'http://localhost:8080/ServletSample/SqlInjection' \
    //--header 'input: D-Artagnan'\'' or '\'''\''='\'''


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            String username = request.getHeader("input");
            danger(username);

            String sql = "Select * from users";
            Connection connection = dataSource.getConnection();
            try (Statement statement = connection.createStatement()) {
                ResultSet resultSet = statement.executeQuery(sql);
                int cid = 0;
                while (resultSet.next()) {
                    response.setHeader("Column " + cid, resultSet.getString(1) + " : " + resultSet.getString(2) + " : " + resultSet.getString(3));
                    cid++;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Test sql injection using Apache Torque BasePeer


    //Valid CURL:
    //curl --location --request POST 'http://localhost:8080/ServletSample/SqlInjection' \
    //--header 'input: D-Artagnan'

    //Malicious CURL:
    //curl --location --request POST 'http://localhost:8080/ServletSample/SqlInjection' \
    //--header 'input: D-Artagnan'\'' or '\'''\''='\'''

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            String sql = "Select * from users";
            initTorque();
            Torque.getConnection("default");
            String username = request.getHeader("input");
            dangerBasePeer(username);
            response.addHeader("Result", BasePeer.executeQuery(sql).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initH2() throws Exception{
        String sqlcreate = "CREATE TABLE users (PersonID int,name varchar(255),status varchar(255));";
        String sqlpop = "INSERT INTO users(PersonID, name, status)VALUES(0, 'D-Artagnan', ''),(1, 'Athos', 'inactive'),(2, 'Porthos', 'inactive'),(3, 'Aramis', 'inactive');";
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sqlcreate);
            statement.executeUpdate(sqlpop);
        }
    }

    private static void initTorque() {
        //Configure Torque with a Local Database:
        //Setup for docker run MySQL Instance - Use below instructions or use the docker compose to create the MySQL server
        //docker run --rm -d --network=mynetwork --name=mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=db -e MYSQL_USER=admin -e MYSQL_PASSWORD=root -p 3306:3306 mysql:latest
        //Ensure that the servlet can access the psql server, if they are both dockerized they must be on the same docker network

        try {
            PropertiesConfiguration config = new PropertiesConfiguration();

            config.setProperty("torque.database.default", "defaultdb");
            config.setProperty("torque.database.defaultdb.adapter", "mysql");
            config.setProperty("torque.dsfactory.defaultdb.factory", "org.apache.torque.dsfactory.SharedPoolDataSourceFactory");
            config.setProperty("torque.dsfactory.defaultdb.connection.driver", "com.mysql.cj.jdbc.Driver");
            config.setProperty("torque.dsfactory.defaultdb.connection.url", "jdbc:mysql://mysql:3306/db");
            config.setProperty("torque.dsfactory.defaultdb.connection.user", "root");
            config.setProperty("torque.dsfactory.defaultdb.connection.password", "root");

            Torque.init(config);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void danger(String input) throws SQLException {
        String sql = "UPDATE users SET status = 'active' WHERE name = '" + input +"'";
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            // ruleid:java_inject_rule-SqlInjection
            statement.executeUpdate(sql);
        }
    }

    public void dangerBasePeer(String input) {
        String sql = "UPDATE users SET status = 'active' WHERE name = '" + input +"'";
        try  {
            // ruleid:java_inject_rule-SqlInjection
            BasePeer.executeStatement(sql);
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

}
