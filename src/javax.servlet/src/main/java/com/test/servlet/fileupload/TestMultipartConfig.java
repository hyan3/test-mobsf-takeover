package com.test.servlet.fileupload;

import java.io.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;



// Rule: java_file_rule-FileUploadFileName
@WebServlet(name = "TestMultipartConfig", value = "/UploadServlet2")
@MultipartConfig
public class TestMultipartConfig extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Part filePart = request.getPart("file"); 
        String fileName = filePart.getSubmittedFileName(); 

        // Write the file to the server
        try (InputStream fileContent = filePart.getInputStream();
                FileOutputStream fos = new FileOutputStream("/data/" + fileName)) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fileContent.read(buffer)) != -1) {
                fos.write(buffer, 0, bytesRead);
            }

            response.getWriter().write("TestMultipartConfig: File uploaded successfully.");
        } catch (Exception e) {
            response.getWriter().write("File upload failed: " + e.getMessage());
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher dispatcher = request.getRequestDispatcher("/uploadTest2.html");
        dispatcher.forward(request, response);
        return;

    }
}
