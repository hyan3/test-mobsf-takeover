package com.test.servlet.crypto;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;

// ref: java_crypto_rule-GCMNonceReuse
public class GcmHardcodedIV extends HttpServlet
{
    public static final int GCM_TAG_LENGTH = 16;
    public static final String IV1String = "ab0123456789";
    public static final byte[] IV1 = IV1String.getBytes();
    public static final byte[] IV2 = new byte[]{97,98,48,49,50,51,52,53,54,55,56,57};

    private static byte[] IV3;

    private static SecretKey secretKey;

    GCMParameterSpec gcmParameterSpec;


    public GcmHardcodedIV() {

        IV3 = IV1String.getBytes();

        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(256);
            secretKey = keyGenerator.generateKey();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String method = req.getParameter("method");
        String input = req.getParameter("input");
        String response;

        switch (method){
            case "encrypt":{
                try {
                    response = encrypt(input);
                }
                catch (Exception e){
                    e.printStackTrace();
                    response = e.getMessage();
                }
                break;
            }
            case "encrypt2":{
                try {
                    response = encrypt2(input);
                }
                catch (Exception e){
                    e.printStackTrace();
                    response = e.getMessage();
                }
                break;
            }
            case "encrypt3":{
                try {
                    response = encrypt3(input);
                }
                catch (Exception e){
                    e.printStackTrace();
                    response = e.getMessage();
                }
                break;
            }
            case "encryptSecure":{
                try {
                    response = encryptSecure(input);
                }
                catch (Exception e){
                    e.printStackTrace();
                    response = e.getMessage();
                }
                break;
            }
            case "decrypt":{
                try {
                    response = decrypt(input);
                }
                catch (Exception e){
                    e.printStackTrace();
                    response = e.getMessage();
                }
                break;
            }
            case "decryptSecure":{
                try {
                    response = decryptSecure(input);
                }
                catch (Exception e){
                    e.printStackTrace();
                    response = e.getMessage();
                }
                break;
            }
            default:{
                response = "Method Not found";
            }
        }

        PrintWriter printWriter = resp.getWriter();
        printWriter.print(response);
        printWriter.close();
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt&input=testinput>'
    public String encrypt(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
        //ruleid: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV1);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt2&input=testinput>'
    public String encrypt2(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
        //ruleid: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV2);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt3&input=testinput>'
    public String encrypt3(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
        //ruleid: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV1String.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encryptSecure&input=testinput>'
    public String encryptSecure(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");

        // Generate a new, random IV for each encryption operation
        SecureRandom secureRandom = new SecureRandom();
        byte[] iv = new byte[12];
        // GCM standard recommends a 12-byte (96-bit) IV
        secureRandom.nextBytes(iv);

        //ok: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        // Prepend the IV to the ciphertext to ensure it's available for decryption
        byte[] cipherTextWithIv = new byte[iv.length + cipherText.length];
        System.arraycopy(iv, 0, cipherTextWithIv, 0, iv.length);
        System.arraycopy(cipherText, 0, cipherTextWithIv, iv.length, cipherText.length);

        return Base64.getEncoder().encodeToString(cipherTextWithIv);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=decrypt&input=S2O2JFva3OaxcMSbVpyZ4wWcrSnF4Lo4bw=='
    public String decrypt(String cipherText) throws Exception {
        cipherText = URLDecoder.decode(cipherText, StandardCharsets.UTF_8.toString()).replace(" ", "+");;
        System.out.println("ENCRYPTED TEXT : " + cipherText);

        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");

        //ruleid: java_crypto_rule-GCMNonceReuse
        gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV3);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] decoded = Base64.getDecoder().decode(cipherText);
        byte[] decryptedText = cipher.doFinal(decoded);

        return new String(decryptedText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=decryptSecure&input=NeI44x0eg1mg+xxsNXzsI4i4YH1XUkELfQnhSZW8v77KPOUE8g=='
    public static String decryptSecure(String encryptedText) throws Exception {
        encryptedText = URLDecoder.decode(encryptedText, StandardCharsets.UTF_8.toString()).replace(" ", "+");
        System.out.println("ENCRYPTED TEXT : " + encryptedText);

        // Decode the base64-encoded string
        byte[] decodedCipherText = Base64.getDecoder().decode(encryptedText);

        // Extract the IV from the beginning of the ciphertext
        byte[] iv = new byte[12];
        System.arraycopy(decodedCipherText, 0, iv, 0, 12);

        // Extract the actual ciphertext
        byte[] cipherText = new byte[decodedCipherText.length - 12];
        System.arraycopy(decodedCipherText, 12, cipherText, 0, cipherText.length);

        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        //ok: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, gcmParameterSpec);

        // Decrypt the ciphertext
        byte[] clearTextBytes = cipher.doFinal(cipherText);

        return new String(clearTextBytes);
    }


}
