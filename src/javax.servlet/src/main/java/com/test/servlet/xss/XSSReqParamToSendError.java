package com.test.servlet.xss;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

import org.apache.commons.text.StringEscapeUtils;
import org.owasp.encoder.Encode;

// ref: java_xss_rule-XSSReqParamToSendError
/* none of the below checks are actually vulnerable to Xss:
% curl -vvv "http://localhost:8080/ServletSample/XSSReqParamToSendError/x?tainted=Test%3cimg%20src=x%20onerror=alert(1)%3e&type=at"
*   Trying [::1]:8080...
* Connected to localhost (::1) port 8080
> GET /ServletSample/XSSReqParamToSendError/x?tainted=Test%3cimg%20src=x%20onerror=alert(1)%3e&type=at HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/8.4.0
> Accept: * /*
> 
< HTTP/1.1 500 
< Set-Cookie: JSESSIONID=05E4F033691E1CC18BB97C991A0BCEB6; Path=/ServletSample; HttpOnly
< Content-Type: text/html;charset=utf-8
< Content-Language: en
< Content-Length: 733
< Date: Wed, 20 Dec 2023 05:11:14 GMT
< Connection: close
< 
* Closing connection
<!doctype html><html lang="en"><head><title>HTTP Status 500 – Internal Server Error</title><style type="text/css">body {font-family:Tahoma,Arial,sans-serif;} h1, h2, h3, b {color:white;background-color:#525D76;} h1 {font-size:22px;} h2 {font-size:16px;} h3 {font-size:14px;} p {font-size:12px;} a {color:black;} .line {height:1px;background-color:#525D76;border:none;}</style></head><body><h1>HTTP Status 500 – Internal Server Error</h1><hr class="line" /><p><b>Type</b> Status Report</p><p><b>Message</b> Test&lt;img src=x onerror=alert(1)&gt;</p><p><b>Description</b> The server encountered an unexpected condition that prevented it from fulfilling the request.</p><hr class="line" /><h3>Apache Tomcat/9.0.84</h3></body></html>

*/
//sample Request: http://localhost:8080/ServletSample/XSSReqParamToSendError/sasa?tainted=Test&type=Test
public class XSSReqParamToSendError extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String paramValue = request.getParameter("type");

        switch (paramValue){
            case "ph":{
                parameterAndHeaderTest(request, response);
                break;
            }
            case "at":{
                attributeTest(request, response);
                break;
            }
            case "mv":{
                parameterMapAndValuesTest(request, response);
                break;
            }case "ps":{
                parameterStringTest(request, response);
                break;
            } default:{
                // ruleid:java_xss_rule-XSSReqParamToSendError
                response.sendError(404, paramValue);
            }
        }
    }

    protected void attributeTest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("attributeName",request.getParameter("tainted"));
        request.getSession().setAttribute("attributeName",request.getParameter("tainted"));
        request.getServletContext().setAttribute("attributeName",request.getParameter("tainted"));

        String requestAttribute = (String) request.getAttribute("attributeName");
        String sessionAttribute = (String) request.getSession().getAttribute("attributeName");
        String contextAttribute = (String) request.getServletContext().getAttribute("attributeName");

        if(requestAttribute != null){
            // ruleid:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, requestAttribute);
        } else if (sessionAttribute != null) {
            // ok:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, Encode.forHtml(sessionAttribute));
        }else if (contextAttribute != null) {
            // ok:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, StringEscapeUtils.escapeHtml3(contextAttribute));
        } else {
            // ok:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, "Hard coded");
        }
    }

    protected void parameterAndHeaderTest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String paramValue = request.getParameter("tainted");
        String header = request.getHeader("tainted");

        if(paramValue != null){
            // ruleid:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, changePara(paramValue));
        }else if(header != null){
            String headerNameEncoded = Encode.forHtml(header);
            // ok:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, headerNameEncoded);
        } else {
            String someString = "some String";
            // ok:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, someString);
        }
    }

    protected void parameterMapAndValuesTest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] parameterValues = request.getParameterValues("tainted");
        Enumeration<String> parameterNames = request.getParameterNames();
        Map<String, String[]> parameterMap = request.getParameterMap();

        String valueFromPV = parameterValues[0];
        String valueFromPN = parameterNames.nextElement();
        String valueFromPM = parameterMap.get("tainted")[0];

        if(valueFromPV != null){
            // ruleid:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, valueFromPV);
        } else if (valueFromPN != null) {
            // ruleid:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, changePara(valueFromPN));
        }else if (valueFromPM != null) {
            // ok:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, StringEscapeUtils.escapeHtml4(valueFromPM));
        } else {
            // ok:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, "Hard coded");
        }
    }

    protected void parameterStringTest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String queryString = request.getQueryString();
        String[] keyValuePairs = queryString.split("=");

        if(keyValuePairs[keyValuePairs.length - 1] != null){
            String lastPair = keyValuePairs[keyValuePairs.length - 1];
            String lastPairEncoded = Encode.forHtml(lastPair);
            // ok:java_xss_rule-XSSReqParamToSendError
            response.sendError(500, lastPairEncoded);
        }else {
            // ok:java_xss_rule-XSSReqParamToSendError
            response.sendError(404);
        }
    }

    public String changePara(String input) {
        return "The Error is " + input;
    }
}