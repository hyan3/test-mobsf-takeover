package com.test.servlet.inject;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// This vulnerability is no longer exploitable as Tomcat and jetty have implemented RFC7230 and RFC3986 Compliance
// Also log4j has implemented fixes for log forging vulnerability.
// Try "Test\r\ning" as an input parameter if required (http://localhost:8080/ServletSample/TestCRLFInjectionLogsApache?input=Test\r\ning)
// Case 1: http://localhost:8080/ServletSample/TestCRLFInjectionLogsApache?input=safe
// Case 2: http://localhost:8080/ServletSample/simpleForm.jsp

public class TestCRLFInjectionLogsApache extends HttpServlet {
    private static final Logger logger = LogManager.getLogger(TestCRLFInjectionLogsApache.class);

    public TestCRLFInjectionLogsApache() {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
        logger.log(Level.INFO, "Testing log4j.Logger - " + input);

        try (PrintWriter out = resp.getWriter()) {
            out.println("RequestParamToHeader Test: " + resp);
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        logger.log(Level.INFO, "Testing log4j.Logger - " + firstName);

        PrintWriter printWriter = resp.getWriter();
        printWriter.print("<html>");
        printWriter.print("<body>");
        printWriter.print("<h1>Student Resistration Form Data</h1>");
        printWriter.print("<p> firstName :: " + firstName + "</p>");
        printWriter.print("<p> lastName :: " + lastName + "</p>");
        printWriter.print("</body>");
        printWriter.print("</html>");
        printWriter.close();

        System.out.println("First Name :: " + firstName);
        System.out.println("Last Name :: " + lastName);
    }
}