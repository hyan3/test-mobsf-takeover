package com.test.servlet.endpoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.net.ssl.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;

// Rule: java_endpoint_rule-HostnameVerifier
public class TestHostnameVerifier extends HttpServlet {

    //Valid Url call using the restricted (google) HostnameVerifier -- Allows Connection
    //curl --location 'http://localhost:8080/ServletSample/TestHostnameVerifier' \
    //--header 'site: https://javaxservlet-trusted-server-1:8443'

    //Invalid Url call using the restricted (google) HostnameVerifier -- Allows connection
    //curl --location 'http://localhost:8080/ServletSample/TestHostnameVerifier' \
    //--header 'site: https://javaxservlet-untrusted-server-1:8443' \
    //--header 'hostnameverifier: allhosts'

    //Invalid Url call using the restricted (google) HostnameVerifier -- Causes Exception
    //curl --location 'http://localhost:8080/ServletSample/TestHostnameVerifier' \
    //--header 'site: https://javaxservlet-untrusted-server-1:8443'

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpsURLConnection connection = null;
        PrintWriter out = resp.getWriter();

        try {

            TrustManager[] trustManagers = new TrustManager[]{new TrustAllManager()};

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagers, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            URL url = new URL(req.getHeader("site"));

            connection = (HttpsURLConnection) url.openConnection();

            connection.setRequestMethod("GET");

            HostnameVerifier hostnameVerifier;

            if ("allhosts".equals(req.getHeader("hostnameverifier"))){
                System.out.println("Using allhosts");
                hostnameVerifier = new AllHosts();
            }
            else{
                System.out.println("Using trusted hosts");
                hostnameVerifier = new TrustedHosts();
            }

            connection.setHostnameVerifier(hostnameVerifier);

            System.out.println("Getting Content");
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                out.println(line);
            }
            reader.close();
        }
        catch (Exception e){
            e.printStackTrace();
        } finally {
            if (connection != null)
                connection.disconnect();
        }
    }
}
class AllHosts implements HostnameVerifier {

    public AllHosts() {
        System.out.println("AllHosts Constructor initialized");
    }

    // ruleid: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        return true;
    }
}

class TrustedHosts implements HostnameVerifier {

    public TrustedHosts() {
        System.out.println("Trusted Hosts Constructor initialized");
    }

    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        return hostname.equals("javaxservlet-trusted-server-1");
    }

}

