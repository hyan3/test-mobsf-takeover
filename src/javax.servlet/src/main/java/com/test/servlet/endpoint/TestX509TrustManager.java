package com.test.servlet.endpoint;

import javax.net.ssl.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

// Rule: java_endpoint_rule-X509TrustManager
public class TestX509TrustManager extends HttpServlet {


    //Valid Url call using the restricted (google) Trust Manager -- Allows Connection
    //curl --location 'http://localhost:8080/ServletSample/TestX509TrustManager' \
    //--header 'site: google.com'

    //Invalid Url call using the restricted (google) Trust Manager -- Allows connection
    //curl --location 'http://localhost:8080/ServletSample/TestX509TrustManager' \
    //--header 'site: microsoft.com'

    //Invalid Url call using the restricted (google) Trust Manager -- Causes Exception
    //curl --location 'http://localhost:8080/ServletSample/TestX509TrustManager' \
    //--header 'site: microsoft.com' \
    //--header 'trustManager: trustall'

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        TrustManager[] trustManagers;

        if ("trustall".equals(request.getHeader("trustManager"))) {
            trustManagers = new TrustManager[]{new TrustAllManager()};
        } else {
            trustManagers = new TrustManager[]{new GoogleOnlyTrustManager()};
        }

        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagers, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

            URL url = new URL("https://" + request.getHeader("site"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            int responseCode = connection.getResponseCode();
            out.println("Response Code: " + responseCode);

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                out.println(line);
            }
            reader.close();

        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
            out.println("Error in SSLContext initialization");
        }
    }
}

class TrustAllManager implements X509TrustManager {

    // ruleid: java_endpoint_rule-X509TrustManager
    @Override
    public void checkClientTrusted(final X509Certificate[] cert, final String authType)
            throws CertificateException {
    }
    // ruleid: java_endpoint_rule-X509TrustManager
    @Override
    public void checkServerTrusted(final X509Certificate[] cert, final String authType)
            throws CertificateException {
    }

    // ruleid: java_endpoint_rule-X509TrustManager
    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}

class GoogleOnlyTrustManager implements X509TrustManager {

    private static final String ALLOWED_HOST = "google.com";

    X509Certificate[] certificates = null;

    // ok: java_endpoint_rule-X509TrustManager
    @Override
    public void checkClientTrusted(X509Certificate[] cert, String authType) throws CertificateException {
        for (X509Certificate certificate : cert){
            certificate.checkValidity();
        }

        certificates = cert;
    }

    // ok: java_endpoint_rule-X509TrustManager
    @Override
    public void checkServerTrusted(X509Certificate[] cert, String authType) throws CertificateException {
        for (X509Certificate certificate : cert) {
            if (certificate.getSubjectX500Principal().getName().contains(ALLOWED_HOST)) {
                return;
            }
        }

        certificates = cert;

        throw new CertificateException("Server certificate is not issued for " + ALLOWED_HOST);
    }

    // ok: java_endpoint_rule-X509TrustManager
    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return certificates;
    }
}
