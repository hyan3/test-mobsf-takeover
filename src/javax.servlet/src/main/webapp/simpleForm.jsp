<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>TestCRLFInjectionLogs</title>
</head>
<body>
<h3>Form to test java.util.logging.Logger</h3>
<form action="TestCRLFInjectionLogs" method="post">
    First Name: <input type="text" name="firstName">
    <br> <br>

    Last Name: <input type="text" name="lastName"><br>

    <br>
    <input type="submit"> <br/>
</form>

<h3>Form to test org.apache.logging.log4j.Logger</h3>
<form action="TestCRLFInjectionLogsApache" method="post">
    First Name: <input type="text" name="firstName">
    <br> <br>

    Last Name: <input type="text" name="lastName"><br>

    <br>
    <input type="submit"> <br/>
</form>

<h3>Form to test org.tinylog.Logger</h3>
<form action="TestCRLFInjectionLogsTiny" method="post">
    First Name: <input type="text" name="firstName">
    <br> <br>

    Last Name: <input type="text" name="lastName"><br>

    <br>
    <input type="submit"> <br/>
</form>

</html>