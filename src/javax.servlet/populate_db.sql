CREATE TABLE users (
PersonID int,
name varchar(255),
status varchar(255)
);

INSERT INTO db.users
(PersonID, name, status)
VALUES
(0, 'D-Artagnan', ''),
(1, 'Athos', 'inactive'),
(2, 'Porthos', 'inactive'),
(3, 'Aramis', 'inactive');