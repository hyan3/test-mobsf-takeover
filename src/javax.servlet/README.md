## javax.servlet

Used for testing web applications source/sinks based on the older javax.servlet namespace.

### Adding tests

Simply create a new package or find the applicable package under `src/main/java/com/gitlab/<type>` to create the new class.
Create the class that demonstrates the vulnerability.

Add the servlet to the src/main/webapp/WEB-INF/web.xml:

```
  ...
  <servlet>
	<servlet-name>TestInject</servlet-name>
	<display-name>TestInject</display-name>
	<description>Test GET, POST methods of Servlet</description>
	<servlet-class>com.test.servlet.inject.TestInject</servlet-class>
  </servlet>

  <servlet-mapping>
	<servlet-name>TestInject</servlet-name>
	<url-pattern>/TestInject</url-pattern>
  </servlet-mapping>
```

### Running

Run:

```
mvn package
docker build -t javaxsample . && docker run --rm -p 8080:8080 --name javaxsample javaxsample
# Once started, hit the endpoint, all servlets are under the "/ServletSample" application namespace.
curl -vvv "http://localhost:8080/ServletSample/TestInject?input=test"
```

To use the in memory H2 Database with java.sql:
```
InitialContext context = new InitialContext();
dataSource = (DataSource) context.lookup("java:comp/env/jdbc/H2DataSource");
```

For testing CookieHttpOnly class:
(Rule: java_cookie_rule-CookieHTTPOnly)

```
Case 1: http://localhost:8080/ServletSample/CookieHttpOnly?input=danger
Case 2: http://localhost:8080/ServletSample/CookieHttpOnly?input=danger2
Case 3: http://localhost:8080/ServletSample/CookieHttpOnly?input=safe
```

For testing CookieInsecure class:
(Rule: java_cookie_rule-CookieInsecure)

```
Case 1: http://localhost:8080/ServletSample/CookieInsecure?input=danger
Case 2: http://localhost:8080/ServletSample/CookieInsecure?input=danger2
Case 3: http://localhost:8080/ServletSample/CookieInsecure?input=safe

```

For testing CookieInsecure class:
(Rule: java_cookie_rule-CookieInsecure)

```
Case 1: curl --location --request POST 'http://localhost:8080/ServletSample/CustomInjection' \--header 'input: D-Artagnan'\'' or '\'''\''='\'''
Case 2: curl --location --request GET 'http://localhost:8080/ServletSample/CustomInjection' \--header 'input: D-Artagnan'\'' or '\'''\''='\'''

```

For testing UnvalidatedRedirect class:
(Rule: java_endpoint_rule-UnvalidatedRedirect)

```
URL: http://localhost:8080/ServletSample/UnvalidatedRedirect
Sample testcase: http://localhost:8080/ServletSample/UnvalidatedRedirect?case=1&redirectTo=https://www.google.com
```

For testing TestSQLInjection class:
(Rule: java_inject_rule-CustomInjectionSQLString)

```
URL: http://localhost:8080/ServletSample/TestSQLInjection?name=Bob
```

For testing FileUploadFileName class:
(Rule: java_file_rule-FileUploadFileName)

Run the docker in interactive mode to see the file uploaded at root instead of /data folder in docker.

```
docker build -t javaxsample . && docker run --rm -d -p 8080:8080 --name javaxsample javaxsample && docker exec -it javaxsample /bin/bash

Test1: http://localhost:8080/ServletSample/UploadServlet
Test2: http://localhost:8080/ServletSample/UploadServlet2
```

To stop the docker:

```
docker stop javaxsample
```

For testing ElInjectionBean class:
(Rule: java_inject_rule-ELInjection)

```
URL: http://localhost:8080/ServletSample/testELInjection.xhtml
```